DoF {
    classvar <>ofHost = "localhost";
    classvar <>ofPort = 12345;
    classvar <netAddr;
    classvar <currentDimmerVal = 1;

    
    *initClass {
        netAddr = NetAddr.new(ofHost, ofPort);
    }

    init {
        netAddr = NetAddr.new(ofHost, ofPort);
    }

    *scene{arg sceneName = "Black";
        netAddr.sendMsg("/scene", sceneName);
    }

    *slitscanBlurScale{arg scale = 0.3;
        netAddr.sendMsg("/slitscan/blurScale", scale);
    }

    // TODO: add time arg to dimmer, so that it fades to target in time
    // Also: add request for current dimmer value from DoF, so that this
    // is always correct.
    *dimmer{ arg value = 1;
        netAddr.sendMsg("/dimmer", value);
        currentDimmerVal = value;
    }

    *dimmerFade{arg to, time;
        Tdef(\dofDimmerFade, {
            var env = Env([currentDimmerVal, to], time).asStream;
            var value;
            loop{
                value = env.next;
                if(env.next != to){
                    netAddr.sendMsg("/dimmer", value);
                }{
                    Tdef(\dofDimmerFade).clear;
                };
                currentDimmerVal = value;
                0.05.wait;
            }
        }).play;
    }

    *toggleParticleSystem{
        netAddr.sendMsg("/camEdgeParticleSystem/toggleParticles");
    }

    *toggleState{
        netAddr.sendMsg("/toggleState");
    }

    *invertColor{
        netAddr.sendMsg("/invertColor");
    }

    *mix{arg mix = 0;
        netAddr.sendMsg("/mix", mix);
    }

    *resetTime{
        netAddr.sendMsg("/resetTime");
    }

    *sprayBurst { arg numParticles = 1000, x = 0, y = 0, z = 0, spread = 20, lifespan = 20, acceleration = 0.01, hue = 125;
        DoF.scene("Spray");
        netAddr.sendMsg("/spray/burst", numParticles, x, y, z, spread, lifespan, acceleration, hue);
    }
}
