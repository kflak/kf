// Abstract superclass for all mbDeltaTrigs. 
// KFMBDeltaTrig implements play and free functions,
KFMBDeltaTrig {

    classvar <emulate = false; // not yet properly implemented. Use MBDataSpoof for now.

    var <db;
    var <speedlim;
    var <threshold;
    var <minAmp;
    var <maxAmp;
    var <fadeTime;
    var <mbIDs;
    var <mbDeltaTrigs;
    var <>freeMBsImmediately = true;
    var <>freeMBsBufferTime = 1;
    var emulator;
    var <fx;
    var <index;


    *new{|db=0, speedlim=0.5, threshold=0.1, minAmp= -40, maxAmp=0, fadeTime=1|
        ^super.newCopyArgs(db, speedlim, threshold, minAmp, maxAmp, fadeTime).init;
    }

    init {
        mbIDs = KF.mb;
        this.prCreateMBDeltaTrigs;
        emulator = TaskProxy.new;
        this.prCreateFx;
    }

    db_{|val| fx.vol = val.dbamp; db = val; }
    speedlim_{|val| mbDeltaTrigs.do(_.speedlim_(val)); speedlim = val}
    threshold_{|val| mbDeltaTrigs.do(_.threshold_(val)); threshold = val}
    minAmp_{|val| mbDeltaTrigs.do(_.minAmp_(val)); minAmp = val}
    maxAmp_{|val| mbDeltaTrigs.do(_.maxAmp_(val)); maxAmp = val}

    volUp {|inc=1| this.db_(db + inc); postf("Current dB: %\n", db)}
    volDown {|dec=1| this.db_(db - dec); postf("Current dB: %\n", db)}

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            "Triggered from superclass".warn;
            [id, dt, minAmp, maxAmp].postln;
        };
    }

    prCreateFx{
        fx = NodeProxy.audio(KF.server, KF.numSpeakers);
        fx.play(fadeTime: fadeTime, vol: db.dbamp);
    }

    testMBFunction {|dt=0.2, minAmp= -20, maxAmp=0, id=9|
        this.mbDeltaTrigFunction.value(dt, minAmp, maxAmp, id);
    }

    startEmulation {|val=true, density=0.2, minDelta=0.1, maxDelta=0.5, minAmp= -20, maxAmp=0, id=9|
        emulator.source = {
            inf.do{
                var d = rrand(minDelta, maxDelta);
                var waitTime;
                this.testMBFunction(d, minAmp, maxAmp, id);
                waitTime = exprand(0.00001, 1.0) * density.clip(0.0, 1.0).reciprocal;
                waitTime.wait;
            }
        };
        emulator.play;
    }

    stopEmulation {
        emulator.clear;
    }

    prCreateMBDeltaTrigs{
        mbDeltaTrigs = mbIDs.collect{|id, idx|
            MBDeltaTrig.new(
                speedlim: speedlim, 
                threshold: threshold,
                minibeeID: id,
                minAmp: minAmp,
                maxAmp: maxAmp,
                function: this.mbDeltaTrigFunction;
            );
        }
    }
    play {|...args|
        var mbs;

        if(args.isEmpty, {
            mbs = mbIDs;
        },{
            mbs = args;
        });

        mbs.do{|id| 
            var index = mbIDs.indexOf(id);
            mbDeltaTrigs[index].play;
        };
    }

    freeMB {|mb|
        var index = mbIDs.indexOf(mb);
        mbDeltaTrigs[index].free;
    }

    prFreeFunc {
        mbDeltaTrigs.do(_.free);
        emulator.clear;
    }

    release{|time|
        var f;
        if(time.isNil){
            f = fadeTime;
        }{
            f = time;
        };
        if(freeMBsImmediately){ this.prFreeFunc };
        fx.free(f);
        SystemClock.sched(f, {
            this.releaseAction;
            fx.clear;
            if(freeMBsImmediately.not){ this.prFreeFunc };
            // KFMBDeltaTrig.all.removeAt(index);
            nil;
        })
    }
    
    free{
        this.release(1);
    }

    releaseAction { }

}
