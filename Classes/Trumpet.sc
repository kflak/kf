Trumpet {

    // TODO: create presets
    var <in, <out, <db;
    var <fadeTime;
    var <numChannels;
    var fx;
    var <reverbMix = 0.3;
    var <room = 0.8;
    var <octaverMix = 0.2;
    var <flangerMix = 0.3;
    var <delayMix = 0.0;
    var <decay = 2;
    var <minDelayTime = 2;
    var <maxDelayTime = 2;
    var <pan = 0;
    var <>numDelays = 4;
    var <>numFlangers = 8;
    var <inputGain = 6;

    *new{
        arg in=2, out, db= 12, fadeTime=1, numChannels=2;
        ^super.newCopyArgs(in, out, db, fadeTime, numChannels);
    }

    reverbMix_{|val| reverbMix = val; fx.set(\reverbMix, reverbMix)}
    room_{|val| room = val; fx.set(\room, room)}
    db_{|val| db = val; fx.set(\amp, db.dbamp)}
    octaverMix_{|val| octaverMix = val; fx.set(\octaverMix, octaverMix)}
    delayMix_{|val| delayMix = val; fx.set(\delayMix, delayMix)}
    flangerMix_{|val| flangerMix = val; fx.set(\flangerMix, flangerMix)}
    maxDelayTime_{|val| maxDelayTime = val; fx.set(\maxDelayTime, maxDelayTime)}
    minDelayTime_{|val| minDelayTime = val; fx.set(\minDelayTime, minDelayTime)}
    inputGain_{|val| inputGain = val; fx.set(\inputGain, inputGain)}
    

    
    play {
        fx = NodeProxy.new.play;
        fx.fadeTime = fadeTime;
        fx.source = {  
            var sig;
            var lag; 
            var numChannels = 2;
            var flanger, verb, hipass, delay;
            var oct, octMix;
            lag = \lag.kr(0.1);

            in = In.ar(\in.kr(in), 1) * \inputGain.kr(inputGain);
            in = Compander.ar(in, in, thresh: -10.dbamp, slopeAbove: 1/8);

            // flanger
            flanger = numFlangers.collect {|i|
                Flanger.ar(
                    in, 
                    delay: 0.01,
                    depth: rrand(1.0, 1.5),
                    feedback: rrand(0.3, 0.4),
                    width: rrand(5, 10),
                    speed: rrand(0.01, 0.1),
                    fratio: 1.0
                ) * 1/numFlangers * 12.dbamp;
            };
            flanger = SplayAz.ar(numChannels, flanger);
            flanger = BPeakEQ.ar(in:flanger, freq:600, rq:5.0, db:-6);
            flanger = Limiter.ar(flanger, -1.dbamp);
            flanger = BHiPass.ar(in:flanger, freq:80, rq:1.0);
            flangerMix = \flangerMix.kr(0.5);
            flanger = (flanger * flangerMix) + (in * (1-flangerMix));

            // delay
            delay = numDelays.collect{|i|
                var d = rrand(\minDelayTime.kr(minDelayTime), \maxDelayTime.kr(maxDelayTime));
                CombC.ar(
                    flanger[i.mod(2)],
                    \maxDelayTime.kr(maxDelayTime),
                    delaytime: d,
                    decaytime: \decay.kr(decay),
                    mul: 1/4,
                );
            };
            delay = SplayAz.ar(numChannels, delay);
            delayMix = \delayMix.kr(0.5);
            delay = (delay * delayMix) + (flanger * (1-delayMix));
            delay = Limiter.ar(delay, -1.dbamp);

            // octaver
            oct = DopplerPitchShift.ar(delay, 0.5);
            octMix = \octaverMix.kr(octaverMix);
            oct = (oct * octMix) + (delay * (1-octMix));

            hipass = BHiPass.ar(in:oct, freq:80, rq:1.0);

            // jpverb
            verb  = FreeVerb.ar(hipass, \reverbMix.kr(reverbMix), room: \room.kr(room));
            verb = FreeVerb.ar(verb);
            sig = verb * \amp.kr(db.dbamp);
        }    
    }

    release{|releaseTime = 1| 
        fx.release(releaseTime);
        SystemClock.sched(
            releaseTime,
            {
                fx.clear;
                nil;
            }
        )
    }
}
