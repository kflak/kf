MBDofSourceSwitcher : KFMBDeltaTrig {

    var <>sources = #[
        "fireplace",
        "sunrain",
        "tunnel",
        "webcam"
    ];

    *new{|db= -6, speedlim=0.5, threshold=0.05, minAmp= -40, maxAmp=0, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            DoF.setSource(sources.choose);
        }
    }
    
}
