MBMotor : KFMBDeltaTrig {

    var <>motor;

    *new { arg db=0, speedlim=2, threshold=0.03, minAmp= -20, maxAmp=0, fadeTime=1;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBMotor;
    }

    initMBMotor{
        globalSpeedlim = speedlim;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            if(motor.notNil){
                if(isPlaying.not){
                    var distance = dt.linlin(0.0, 1.0, -100, -800);
                    motor.move(distance);
                    SystemClock.sched(globalSpeedlim, {
                        isPlaying = false;
                    })
                }
            }
        }
    }
}
