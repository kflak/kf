Mdef {
    var <key;
    classvar <all;

    *initClass {
        all = Dictionary.new;
    }

    *new{|key, item|
		var res = all.at(key);
		if(res.isNil) {
			res = all.put(key, item);
		} {
            all.at(key).free;
            res = all.put(key, item);
        }
		^res
    }

    *play{|key|
        all.at(key).play;
    }
}
