MBArvo : KFMBDeltaTrig {

    var <>rateArray = #[1, -0.5, 0.25];
    var <distMix = 0.0;
    var <reverbMix = 0.4;
    var <room = 0.8;

    *new { arg db=0, speedlim=0.5, threshold=0.03, minAmp= -20, maxAmp=0, fadeTime=1;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBArvo;
    }

    initMBArvo{
        KF.loadBuffersSingleDir("~/mySamples/arvoBetter/");
        fx.filter(1, {|in|
            var lag = \lag.kr(0.1);
            var sig;
            var dMix, verbMix;
            var dist, verb, locut, hishelf, out;
            in = in * \inputgain.kr(30.dbamp);

            dist = in * LFNoise1.ar(LFNoise1.kr(0.4).range(400, 800));
            dMix = \distMix.kr(distMix, lag);
            dist = (1 - dMix) * in + (dist * dMix);

            verb = FreeVerb.ar(dist, \reverbMix.kr(reverbMix), \room.kr(room));
            verb = FreeVerb.ar(verb);

            locut = BHiPass.ar(in: verb, freq:120.0);
            hishelf = BHiShelf.ar(in: locut, freq:400, rs:1.0, db: -6);

            sig = hishelf * \amp.kr(db.dbamp);
        });
    }

    distMix_{|val| distMix = val; fx.set(\distMix, distMix)}
    reverbMix_{|val| reverbMix = val; fx.set(\reverbMix, reverbMix)}
    room_{|val| room = val; fx.set(\room, room)}
    db_{|val| db = val; fx.set(\amp, db.dbamp)}


    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(KF.buf[\arvoBetter]),
                \dur, dt.linlin(0.0, 1.0, 0.5, 2),
                \attack, Pkey(\dur) * 0.2,
                \release, Pkey(\dur) * Pwhite(1.0, 2.0),
                \rate, Pwrand(
                    rateArray, 
                    [1.0, 0.5, 0.25].normalizeSum,
                    inf
                ),
                \rateDev, Pwhite(0, 0.001),
                \posDev, Pwhite(0, 0.01),
                \playbackRate, Pwrand(
                    [-0.5, 0.25, 0.25],
                    [0.5, 0.25, 0.3].normalizeSum,
                    inf
                ),
                \grainsize, Pwhite(0.01, 0.8),
                \grainfreq, Pwhite(4, 20),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
                \panDev, Pwhite(0.0, 1.0),
            );
        }
    }

    releaseAction {
        KF.freeBuffersSingleDir(\arvoBetter);
    }
}
