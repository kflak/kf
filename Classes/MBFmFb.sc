MBFmFb : KFMBDeltaTrig {

    *new { arg db=0, speedlim=0.5, threshold=0.03, minAmp= -20, maxAmp=0, fadeTime=1;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBFmFb;
    }

    initMBFmFb {
        fx.filter(1, {|in|
            var sig = in;
            var verb, verbMix=0.4;
            var maxdelay, maxrate, dsig, mixed, mix, local, lag; 
            sig = BHiPass.ar(sig, 60);
            verb = JPverbMono.ar(in, size: 1.0, t60: 3);
            sig = (1 - verbMix) * sig + (verb * verbMix);
            lag = \lag.kr(0.1);
            mix = \mix.kr(0.3, lag);
            maxdelay = 0.013; 
            maxrate = 10.0; 
            local = LocalIn.ar(KF.numSpeakers); 
            dsig = AllpassL.ar(
                in + (local * \feedback.kr(0.0, lag)), 
                maxdelay * 2, 
                LFPar.kr( 
                    \rate.kr(0.06, lag) * maxrate, 
                    0, 
                    \depth.kr(0.08, lag) * maxdelay, 
                    \delay.kr(0.1, lag) * maxdelay
                ), 
                \decay.kr(0.0, lag)
            ); 
            mixed = in + dsig;
            LocalOut.ar(mixed); 
            sig = (1 - mix) * in + (mixed * mix);
            sig;
        })
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \pmFB,
                \dur, dt.linlin(0.0, 1.0, 0.1, 2),
                \octave, Pwhite(2, 5, 1),
                \degree, 1,
                \modfreq, Pfunc{|ev| ev.use { ~freq.() + rrand(-10.0, 10.0) }},
                \modFeedback, Pwhite(0.01, 1.0),
                \attack, Pwhite(2.0, 7),
                \modattack, Pkey(\attack) * 2,
                \release, dt.linlin(0.0, 1.0, 2.5, 15),  
                \modrelease, Pkey(\release),
                \pmindex, Pwhite(7, 13),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
            );
        }
    }

    releaseAction {
        // KF.freeBuffersSingleDir(\angelKlli);
    }
}
