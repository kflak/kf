MBDofSwitcher : KFMBDeltaTrig {

    var task;
    var <>scenes = #[
        "BoxPhysarum",
        "Caleidoscope",
        "CamEdge",
        "CamEdgeParticleSystem",
        "CamFire",
        "CamGlitch",
        "CamTunnel",
        "CircleDisplay",
        "CleanPhysarum",
        "Instructions",
        "SlitScan",
        "Spray",
        "Sticks",
        "Text",
        "Tunnel",
    ];

    *new{|db= -6, speedlim=0.5, threshold=0.05, minAmp= -40, maxAmp=0, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBDofSwitcher;
    }

    initMBDofSwitcher { 
        task = TaskProxy.new();
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var scene = scenes.choose;
            DoF.scene(scene);
            switch(scene,
                "BoxPhysarum", {},
                "Caleidoscope", {},
                "CamEdge", {},
                "CamEdgeParticleSystem", {},
                "CamFire", {},
                "CamGlitch", {},
                "CamTunnel", {},
                "CleanPhysarum", {},
                "CircleDisplay", {},
                "Instructions", {},
                "SlitScan", {},
                "Spray", {},
                "Sticks", {},
                "Text", {},
                "Tunnel", {},
            )
        }
    }

    free {
        super.free;
        task.clear;
    }
}
