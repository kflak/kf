MBIceCrack : KFMBDeltaTrig {

    var <reverbMix = 0.3;
    var <>conv = false;
    var <>direction = 1;

    *new { arg db=0, speedlim=0.5, threshold=0.1, minAmp= -20, maxAmp=0, fadeTime=20;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBIceCrack;
    }

    initMBIceCrack{
        // KF.loadBuffersSingleDir("~/mySamples/IceCrack/");
        // KF.loadBuffersSingleDir("~/mySamples/IceCrackConv/");
        fx.filter(1, {|in|
            var lag = \lag.kr(0.1);
            var sig;
            var verb, verbMix;

            sig = in * \inputgain.kr(0.dbamp);

            verbMix = \reverbMix.kr(reverbMix);
            verb = JPverb.ar(sig, size: 1.0, t60: 1.0);

            sig = (1 - verbMix) * sig + (verb * verbMix);
            sig = BHiPass.ar(in: sig, freq: 60.0);

            sig = sig * \amp.kr(db.dbamp);
        });
    }

    reverbMix_{|val| reverbMix = val; fx.set(\reverbMix, reverbMix)}
    db_{|val| db = val; fx.set(\amp, db.dbamp)}


    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var buf = if(conv){ KF.buf[\IceCrackConv]}{ KF.buf[\IceCrack] };
            fx[0] = Pbind(
                \instrument, \granulator,
                \buf, Prand(buf),
                \dur, Pfunc{|ev| ev.buf.duration},
                \attack, 0.01,
                \release, 0.1,
                \rate, 1,
                \posRate, Pwhite(0.8, 1.0) * direction,
                \pan, Pwhite(-1.0, 1.0),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
            )
        }
    }
}
