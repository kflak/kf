MBTrumpetulator : KFMBDeltaTrig {

// ERROR: check why I get 4 channels here...

    var <reverbMix = 0.4;
    var <room = 0.8;
    var <flangerMix = 0.3;
    var <delayMix = 0.0;

    *new{|db=0, speedlim=0.5, threshold=0.035, minAmp= -12, maxAmp=6, fadeTime=10|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBTrumpetulator;
    }

    initMBTrumpetulator {
        KF.loadBuffersSingleDir("~/mySamples/BrokenPiano/");
        KF.loadBuffersSingleDir("~/mySamples/trumpet/");
        "MBTrumpetulator initialized".postln;
    fx.filter(1, {|in|  
        var sig;
        var numChannels = KF.numSpeakers;
        var numFlangers = 8;
        var flanger, verb, hipass, delay;
        var oct, octMix;

        in = Compander.ar(in, in, thresh: -10.dbamp, slopeAbove: 1/8);

        // flanger
        flanger = numFlangers.collect {|i|
            Flanger.ar(
                in, 
                delay: 0.01,
                depth: rrand(1.0, 1.5),
                feedback: rrand(0.3, 0.4),
                width: rrand(5, 10),
                speed: rrand(0.01, 0.1),
                fratio: 1.0
            ) * 1/numFlangers * 12.dbamp;
        };
        flanger = SplayAz.ar(numChannels, flanger);
        flanger = BPeakEQ.ar(in:flanger, freq:600, rq:5.0, db:-6);
        flanger = Limiter.ar(flanger, -1.dbamp);
        flanger = BHiPass.ar(in:flanger, freq:80, rq:1.0);
        flangerMix = \flangerMix.kr(0.5);
        flanger = (flanger * flangerMix) + (in * (1-flangerMix));

        // freeverb
        verb  = FreeVerb.ar(flanger, \reverbMix.kr(reverbMix), room: \room.kr(room));
        verb = FreeVerb.ar(verb);
        sig = verb * 30.dbamp;
    });
    }

    reverbMix_{|val| reverbMix = val; fx.set(\reverbMix, reverbMix)}
    room_{|val| room = val; fx.set(\room, room)}
    delayMix_{|val| delayMix = val; fx.set(\delayMix, delayMix)}
    flangerMix_{|val| flangerMix = val; fx.set(\flangerMix, flangerMix)}


    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \grbufphasor,
                \buf, Prand((KF.buf[\BrokenPiano]++KF.buf[\trumpet])),
                \dur, dt.linlin(0.0, 1.0, 0.5, 1),
                \attack, Pkey(\dur) * 0.2,
                \release, Pkey(\dur) * 2,  
                \rate, Prand([0.25, 1], inf),
                \rateDev, Pwhite(0.0, 0.15),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 2),
                \grainsize, Pwhite(0.001, 0.15),
                \grainfreq, Pwhite(1, 12),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \panDev, Pwhite(0.0, 1.0),
            );
        }
    }

    releaseAction {
        KF.freeBuffersSingleDir(\trumpet);
        KF.freeBuffersSingleDir(\BrokenPiano);
    }

}
