MBHighMetals : KFMBDeltaTrig {    

    var <>rateArray = #[1, -0.5, 0.25];
    var <reverbMix = 0.4;
    var <room = 0.5;
    var <>filterMB = 9;
    var <>minFilterFreq = 4000;
    var <>maxFilterFreq = 15000;
    var currentPos = 0;

    *new { arg db=0, speedlim=0.5, threshold=0.03, minAmp= -20, maxAmp=0, fadeTime=1;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBHighMetals;
    }

    initMBHighMetals{
        KF.loadBuffersSingleDir("~/mySamples/verkstedshallen/");
        fx.filter (1, {|in|
            var lag = \lag.kr(0.1);
            var comb, combMix, combBus, verbMix, filterBus, locutFreq;
            var verb, locut, hishelf, sig;

            in = in * \inputgain.kr(0.dbamp);

            filterBus = KF.mbData[filterMB].xbus.kr.lag(0.1);
            locutFreq = filterBus.linexp(0.0, 1.0, minFilterFreq, maxFilterFreq);
            locut = BHiPass.ar(in, locutFreq);

            hishelf = BHiShelf.ar(locut, 400, db: -6);

            comb = CombC.ar(
                hishelf, 
                maxdelaytime: 0.005, 
                delaytime: LFNoise1.kr(1).range(0.002, 0.004).lag(1),
                decaytime: 1
            );
            combBus = KF.mbData[filterMB].deltabus.kr.lag(0.1);
            combMix = \combMix.kr(combMix, lag);
            comb = (1 - combMix) * hishelf + (comb * combMix);

            verb = FreeVerb.ar(comb, \reverbMix.kr(reverbMix), \room.kr(room));
            verb = FreeVerb.ar(verb);

            hishelf = BHiShelf.ar(in: verb, freq:600, db: -3);

            sig = hishelf * \amp.kr(db.dbamp, lag);
        } );
    }

    reverbMix_{|val| reverbMix = val; fx.set(\reverbMix, reverbMix)}
    room_{|val| room = val; fx.set(\room, room)}

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var buf = KF.buf[\verkstedshallen][0];
            var numFrames = buf.numFrames;
            var dur = 0.2;
            var step = dur * KF.server.sampleRate;
            var len = dt.linlin(0.0, 1.0, step, step * 20);
            var pos = (currentPos, currentPos+step..currentPos+len);
            currentPos = pos[pos.size-1].mod(numFrames);

            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, buf,
                \dur, dur,
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * 4,
                \startPos, Pseq(pos),
                \legato, 2,
                \rate, Pfunc{ KF.mbData[filterMB].x.linexp(0.0, 1.0, 2.0, 16.0)},
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
            );
        }
    }

    releaseAction {
        KF.freeBuffersSingleDir(\verkstedshallen);
    }
}
