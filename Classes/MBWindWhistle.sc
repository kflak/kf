MBWindWhistle : KFMBDeltaTrig {

    var <distMix = 0.0;
    var <reverbMix = 0.2;
    var <reverbTime = 5;

    *new { arg db=0, speedlim=0.5, threshold=0.03, minAmp= -20, maxAmp=0, fadeTime=1;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBWindWhistle;
    }

    initMBWindWhistle{
        KF.loadBuffersSingleDir("~/mySamples/windWhistle/");
        fx.filter(1, {|in|
            var lag = \lag.kr(0.1);
            var dMix, verbMix, verb;
            var sig;
            in = in * \inputgain.kr(30.dbamp);

            sig = in * LFNoise1.ar(LFNoise1.kr(0.4).range(400, 800));
            dMix = \distMix.kr(distMix, lag);
            sig = (1 - dMix) * in + (sig * dMix);

            verbMix = \verbMix.kr(reverbMix);
            verb = JPverbMono.ar(sig, size: 3, t60: \verbTime.kr(reverbTime));
            sig = (1 - verbMix) * sig + (verb * verbMix);

            sig = BHiPass.ar(in: sig, freq:120.0);
            sig = BHiShelf.ar(in: sig, freq:400, rs:1.0, db: -12);

            sig = sig * \amp.kr(db.dbamp);
        });
    }

    distMix_{|val| distMix = val; fx.set(\distMix, distMix)}
    reverbMix_{|val| reverbMix = val; fx.set(\verbMix, reverbMix)}
    reverbTime_{|val| reverbTime = val; fx.set(\verbTime, reverbTime)}
    db_{|val| db = val; fx.set(\amp, db.dbamp)}


    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(KF.buf[\windWhistle]),
                \dur, dt.linlin(0.0, 1.0, 0.5, 4),
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * Pwhite(1.0, 2.0),
                \rate, Pexprand(0.25, 1.0, 1),
                \rateDev, Pwhite(0, 0.001),
                \posDev, Pwhite(0, 0.01),
                \playbackRate, Pexprand(0.125, 0.5),
                \grainsize, Pwhite(0.01, 0.8),
                \grainfreq, Pwhite(4, 20),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
                \panDev, Pwhite(0.0, 1.0),
            );
        }
    }

    releaseAction {
        KF.freeBuffersSingleDir(\windWhistle);
    }
}
