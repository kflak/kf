MBAngel : KFMBDeltaTrig {

    *new { arg db=0, speedlim=0.5, threshold=0.03, minAmp= -20, maxAmp=0, fadeTime=1;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBAngel;
    }

    initMBAngel {
        KF.loadBuffersSingleDir("/home/kf/mySamples/angelKlli/");
        fx.filter(1, {|in|
            var sig = in;
            var verb, mix=0.5;
            verb = JPverbMono.ar(in, size: 1.0, t60: 4);
            sig = (1 - mix) * sig + (verb * mix);
            sig = BHiPass.ar(sig, 120);
            sig = BHiShelf.ar(sig, 500, db: -6);
            sig = sig * 30.dbamp;
            sig;
        })
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var startPos, currentFx, buf, numFrames,
            grainsize, numGrains, step, len, pos, rate, duration,
            attack, release, legato;
            buf = KF.buf[\angelKlli].choose;
            numFrames = buf.numFrames;
            grainsize = 0.2;
            numGrains = 20;
            step = grainsize * KF.server.sampleRate;
            //in samples, not seconds....
            len = dt.linlin(0.0, 1.0, step, step * numGrains);
            startPos = rrand(0, numFrames - len);
            pos = (startPos, startPos+step..startPos+len);
            startPos = pos[pos.size-1].mod(numFrames);
            rate = 1;
            duration = len/KF.server.sampleRate;
            attack = rrand(0.2, 0.5);
            release = duration - attack;
            legato = 2;
            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, buf,
                \dur, grainsize,
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * 2,
                \startPos, Pseq(pos),
                \legato, legato,
                \rate, Pwhite(0.99, 1.01),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp)
                + Pseg([-70, 0, -70], [attack, release]),
                \pan, Pwhite(-1.0, 1.0),
            );
        }
    }

    releaseAction {
        // KF.freeBuffersSingleDir(\angelKlli);
    }
}
