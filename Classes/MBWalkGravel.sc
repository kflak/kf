MBWalkGravel : KFMBDeltaTrig {

    *new { arg db=0, speedlim=0.5, threshold=0.05, minAmp= -6, maxAmp=6, fadeTime=1;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBWalkGravel;
    }

    initMBWalkGravel{
        KF.loadBuffersSingleDir("~/mySamples/walkGravel/");
        fx.filter(1, {|in|
            var sig = BHiShelf.ar(in, 600, db: -9);
            var verb, verbMix = 0.3;
            verb = JPverbMono.ar(sig, size: 1.0, t60: 2);
            sig = (1 - verbMix) * sig + (verb * verbMix);
            sig = sig = Compander.ar(sig, sig, -6.dbamp, slopeAbove: 1/4);
            sig = sig * 6.dbamp;
        });
    }


    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(KF.buf[\walkGravel]),
                \dur, Prand((0.5..2.0), inf),
                \rate, Prand([0.5, 1], inf),
                \rateDev, Pwhite(0.0, 0.5),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 2),
                \grainsize, Pwhite(0.01, 0.5),
                \grainfreq, Pwhite(2, 40),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \panDev, Pwhite(0.0, 1.0),
                \attack, Pkey(\dur) / 2,
                \release, Pkey(\dur),
                \pan, Pwhite(-1.0, 1.0),
            );
        }
    }

    releaseAction {
        KF.freeBuffersSingleDir(\walkGravel);
    }
}
