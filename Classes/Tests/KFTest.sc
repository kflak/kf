KFTest1 : UnitTest {
	test_check_classname {
		var result = KF.new;
		this.assert(result.class == KF);
	}
}


KFTester {
	*new {
		^super.new.init();
	}

	init {
		KFTest1.run;
	}
}
