MBWindy : KFMBDeltaTrig {
    var <reverbTime = 3;
    var <reverbMix = 0.6;
    var <>minFreq = 80;
    var <>maxFreq = 20000;

    *new{|db=0, speedlim=0.5, threshold=0.03, minAmp= -60, maxAmp= -20, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBWindy;
    }

    initMBWindy{
        "MBWindy initialized".postln;
        fx.filter(1, {|in|
            var lag = \lag.kr(0.5);
            var verb = JPverbMono.ar(in, t60: \reverbTime.kr(reverbTime));
            var verbMix = \reverbMix.kr(reverbMix);
            var sig = (1 - verbMix) * in + (verb * verbMix);
            sig = sig * \amp.kr(db.dbamp, lag);
            sig;
        });
    }

    reverbTime_ {|val| reverbTime = val; fx.set(\reverbTime, reverbTime)}
    reverbMix_ {|val| reverbMix = val; fx.set(\reverbMix, reverbMix)}


    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|
            var attack = dt.linlin(0.0, 1.0, 3, 2);
            var release = dt.linlin(0.0, 1.0, 1, 3);
            var length = attack + release;
            var db = dt.linlin(0.0, 1.0, minAmp, maxAmp);

            fx[0] = Pmono(
                \pink,
                \db, Pseg([-70, db, -70], [attack, release]),
                \dur, 0.1,
                \hicut, Pfunc{ KF.mbData[id].x.linexp(0.0, 1.0, minFreq, maxFreq) },
            );
        }
    }
}
