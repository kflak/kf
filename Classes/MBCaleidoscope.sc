MBCaleidoscope {
    var <>segmentMB = 9;
    var <>rotationMB = 10;
    var <>minSegments = 8;
    var <>maxSegments = 40;
    var <>minRotation = -0.001;
    var <>maxRotation = -0.6;
    var task;

    *new{|segmentMB = 9, rotationMB = 10|
        ^super.newCopyArgs(segmentMB, rotationMB).init;
    }

    init {
        DoF.scene("Caleidoscope");
        task = TaskProxy.new({             
            inf.do({
                var numSegments = KF.mbData[segmentMB].x.linlin(0.0, 1.0, minSegments, maxSegments);
                var rotation = KF.mbData[rotationMB].x.linlin(0.0, 1.0, minRotation, maxRotation);
                numSegments = numSegments.round(2).clip(8, maxSegments);
                DoF.setSegments(numSegments);
                DoF.setRotation(rotation);
                0.1.wait;
            })
        });
    }

    play {
        task.play;
    }

    free {
        task.clear;
    }
}
