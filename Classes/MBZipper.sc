MBZipper : MBShuffleGranulator {


    *new{|db= -10, speedlim=0.3, threshold=0.02, minAmp= -6, maxAmp=0, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBZipper;
    }

    initMBZipper {
        KF.loadBuffersSingleDir("~/mySamples/zipper/");
        freeMBsImmediately = false;
        loop = true;
        buf = KF.buf[\zipper];
        stride = 0.3;
        minDuration = 0.2;
        maxDuration = 2;
        attackMul = 0.7;
        releaseMul = 5;
        fx.filter (1, {|in|
            var lag = \lag.kr(0.1);
            var sig;
            var dMix, verbMix=0.1;
            var dist, verb, locut, hishelf, out;
            sig = in * \inputgain.kr(20.dbamp);
            verb = JPverb.ar(in, t60: 2);
            sig = (1 - verbMix) * sig + (verb * verbMix);
            sig = Compander.ar(sig, sig, -10.dbamp, slopeAbove: 1/8);
            sig = BHiPass.ar(in: sig, freq: LFNoise2.kr(1/4).range(80, 400));
            sig = BHiShelf.ar(in: sig, freq: 500, db: -3);
            sig = sig * \amp.kr(db.dbamp);
        });
    }

    releaseAcion {
        KF.freeBuffersSingleDir(\zipper);
    }
}
