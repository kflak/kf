MBTreePerculator : KFMBDeltaTrig {

    *new{|db= -6, speedlim=0.5, threshold=0.1, minAmp= -6, maxAmp=0, fadeTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBTreePerculator;
    }

    initMBTreePerculator {
        KF.loadBuffersSingleDir("~/mySamples/treeperc/");
    }


    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, Prand(KF.buf[\treeperc], inf),
                \rate, 2,
                \dur, Pseq((0.125!4)),
                \release, Pkey(\dur) * 2,  
                \legato, 0.1,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
            );
        }
    }
}

