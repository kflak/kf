MBGrainDelay : KFMBDeltaTrig {

    var <in=2;
    var <>maxDelay = 10;
    var <>minGrainDur = 0.01, <>maxGrainDur = 0.1;
    var <minDur = 1, <maxDur = 4;
    var <>minRate = 1, <>maxRate = 1;
    var <>overlap = 2;
    var <buf;
    var <recId;
    var <recPos;
    var recorder;
    var setRecPos;
    var <monitordb = 1;
    var <>minRelease = 3, <>maxRelease = 8;
    var <>minAttack = 0, <>maxAttack = 1;
    var <reverbMix = 0.2, <reverbTime = 3;
    var <inputgain = 0;
    var bufFreed = true;

    *new{|db=0, speedlim=0.5, threshold=0.01, minAmp= -40, maxAmp=0, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBGrainDelay;
    }

    initMBGrainDelay{
        buf = Buffer.alloc(KF.server, maxDelay * KF.server.sampleRate);
        bufFreed = false;
        this.prCreateRecorder;
        this.prCreateOscFunc;
        "MBGrainDelay initialized".postln;
        fx.filter(1, {|in|
            var verb, verbMix, sig;
            in = in * \inputgain.kr(inputgain.dbamp);
            verb = JPverbMono.ar(in, t60: \reverbTime.kr(reverbTime));
            verbMix = \reverbMix.kr(reverbMix);
            sig = (1 - verbMix) * in + (verb * verbMix);
            sig = sig * \amp.kr(db.dbamp, \lag.kr(0.1));
            sig;
        })
    }

    inputgain_{|val| inputgain = val; fx.set(\inputgain, inputgain.dbamp)}
    monitordb_{|val| monitordb = val; recorder.set(\amp, monitordb.dbamp)}
    in_{|val| in = val; recorder.set(\in, in)}
    reverbMix_{|val| reverbMix = val; fx.set(\reverbMix, reverbMix)}
    reverbTime_ {|val| reverbTime = val; fx.set(\reverbTime, reverbTime)}


    prCreateRecorder {
        recId = rrand(0, 10000);
        recorder = Synth(\rec, [
            \in, in,
            \id, recId,
            \buf, buf,
            \amp, monitordb.dbamp,
        ]);
    }

    prCreateOscFunc {
        setRecPos = OSCFunc( {|msg, time|
            if(msg[2]==recId){
                recPos = msg[3];
            };
        }, '/recPos');
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            if(fx.isPlaying && buf.notNil){
                var release = dt.linlin(0.0, 1.0, minRelease, maxRelease);
                var attack = dt.linlin(0.0, 1.0, minAttack, maxAttack);
                var dbMax = dt.linlin(0.0, 1.0, minAmp, maxAmp);
                var db = Pseg([-70, dbMax, -70], [attack, release]);
                var graindur = exprand(minGrainDur, maxGrainDur);

                fx[0] = Pfindur(
                    dt.linlin(0.0, 1.0, minDur, maxDur),
                    Pbind(
                        \instrument, \playbuf,
                        \buf, buf,
                        \loop, 1,
                        \db, db,
                        \rate, Pfunc{ 
                            var bus = KF.mbData[id].xbus.getSynchronous;
                            var rate = bus.linlin(0.0, 1.0, minRate, maxRate);
                            rate;
                        },
                        \dur, graindur,
                        \attack, Pkey(\dur),
                        \release, Pkey(\dur),
                        \legato, overlap,
                        \pan, Pwhite(-1.0, 1.0),
                        \startPos, Pfunc{|ev|
                            var delayTime = rrand((ev.dur * ev.legato), ev.buf.duration);
                            var startPos = recPos - (delayTime * KF.server.sampleRate);
                            startPos = startPos.mod(ev.buf.numFrames)
                        },
                    )
                )
            }
        }
    }

    release{|time=5|
        super.release(time);
        fork{
            recorder.release(time);
            (time+1).wait;
            buf.free({bufFreed = true});
            buf = nil;
            setRecPos.clear;
        }
    }
}
