KF {
    classvar <>numInputBusChannels = 2;
    classvar <>numOutputBusChannels;
    classvar <numSpeakers = 2;
    classvar <numSubs = 0;
    classvar <packageRoot;
    classvar <server, options;
    classvar <mbData;
    classvar <>mb = #[9, 10, 11, 12, 13, 14, 15, 16];
    classvar <buf;
    classvar <mainLevel = 1.0;
    classvar <subLevel = 0.316;
    classvar <group, <mainBus, <main, <sub;
    classvar <>audioDir;
    classvar <bufCounter;

    *initClass{
        server = server ?? { Server.default };
        packageRoot = KF.filenameSymbol.asString.dirname.dirname;
        bufCounter = Dictionary.new;
    }

    *start { | doWhenBooted |
        var langPort = NetAddr.langPort;
        mbData = IdentityDictionary.new;
        mb.do{|id| mbData.put(id, MBData.new(id))};
        MBDeltaTrig.mbData = mbData;
        MBXYPad.mbData = mbData;
        MBDeltaLagUD.mbData = mbData;

        options = server.options;
        options.numInputBusChannels = numInputBusChannels;
        if(numOutputBusChannels.isNil){
            options.numOutputBusChannels = numSpeakers + numSubs;
        }{
            options.numOutputBusChannels = numOutputBusChannels;
        };
        server.latency = 0.1;
        server.waitForBoot({

            KFSynthDefs.new(numSpeakers);

            if(numSubs > 0){
                sub = NodeProxy.new.play;
                sub.source = {
                    var in = In.ar(0);
                    Out.ar(numSpeakers, in * \subLevel.kr(subLevel));
                }
            };
            postf("Language port is %\n", langPort);
            doWhenBooted.value();
        });
        switch (audioDir.class,
            String, {
                KF.loadBuffers(audioDir, \mono);
            },
            Array, {
                KF.loadBuffers(audioDir[0], \mono);
                KF.loadBuffers(audioDir[1], \multichannel);
            }
        );
    }

    *loadBuffers {|path, chan|
        server.doWhenBooted({
            var audioDirCount=0;
            if(audioDir.notNil){
                PathName((path).standardizePath).folders.do({|folder|
                    KF.loadBuffersSingleDir(folder, chan);
                    audioDirCount = audioDirCount + 1; 
                });
                "% % directories loaded\n".postf(audioDirCount, chan);
            }{
                "Audio directory not found".warn;
            };
        });
    }

    *loadBuffersSingleDir {|path, chan = \mono |
        var folder, key;
        if(path.class==PathName){
            folder = path;
        }{
            folder = PathName(path.standardizePath);
        };
        if(buf.isNil){
            buf = Dictionary.new;
        };
        key = folder.folderName.asSymbol;
        if(buf[key].isNil) {
            if(chan == \mono){
                buf[key] = SoundFile.collectIntoBuffersMono(folder.fullPath+/+"*");
            }{
                buf[key] = SoundFile.collectIntoBuffers(folder.fullPath+/+"*");
            };
            bufCounter[key] = 1;
        }{
            // if more than one class is using the same buffer dir
            bufCounter[key] = bufCounter[key] + 1;
        };
        folder.fullPath.postln;
    }

    *reloadSynthDefs{
        KFSynthDefs.new(numSpeakers);
    }

    *numSpeakers_ {|val|
        numSpeakers = val;
        this.prSetNumOutputBusChannels;
    }

    *numSubs_ {|val|
        numSubs = val;
        this.prSetNumOutputBusChannels;
    }

    *prSetNumOutputBusChannels {
        server.options.numOutputBusChannels = numSpeakers + numSubs;
    }

    *mainLevel_ {|level|
        mainLevel = level;
        main.set(\amp, mainLevel);
    }

    *subLevel_ {|level|
        subLevel = level;
        sub.set(\amp, subLevel);
    }

    *speakerTest {|amp=0.1|
        (
            fork{ var dur = 1;
                KF.numSpeakers.do{|i, idx| 
                    {Out.ar(
                        idx, SinOsc.ar(880) * XLine.kr(amp, 0.00001, dur, doneAction: 2)
                    )}.play;
                    dur.wait
                };
                // test subs
                KF.numSubs.do{|i, idx|
                    {Out.ar(
                        KF.numSpeakers + i, SinOsc.ar(88) * XLine.kr(0.3, 0.00001, 4, doneAction: 2)
                    )}.play;
                    1.wait;
                }
            };
        )
    }

    *freeBuffers {
        buf.do{|i|
            i.do(_.free);
        };
        buf = nil;
    }

    *freeBuffersSingleDir {|key|
        // make sure that any buffer in use by another class is not
        // accidentally freed up...
        if(bufCounter[key] > 1){
            bufCounter[key] = bufCounter[key] - 1;
        }{
            buf[key].do(_.free);
            buf[key] = nil;
            bufCounter[key] = nil;
        }
    }
}
