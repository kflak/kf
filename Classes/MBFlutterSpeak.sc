MBFlutterSpeak : KFMBDeltaTrig {

    var lowRateBegin = 1;
    var lowRateTarget = 0.125;
    var lowRate = 1;
    var highRateBegin = 4;
    var highRateTarget = 0.25;
    var highRate = 4;
    var rateTime = 120;
    var lowRateEnv;
    var highRateEnv;

    *new { arg db=0, speedlim=0.5, threshold=0.03, minAmp= -20, maxAmp=0, fadeTime=1;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBFlutterSpeak;
    }

    initMBFlutterSpeak {
        KF.loadBuffersSingleDir("~/mySamples/flutterSpeak/");
        fx.filter(1, {|in|
            var sig;
            // var delayTimes = [0.2, 0.5, 0.7];
            // var delayMix = 0.3;
            // var delay = 3.collect{|i|
            //     CombC.ar(in, maxdelaytime: 0.7, delaytime: delayTimes[i], decaytime: 1.0) / 3;
            // };
            var verb, verbMix = 0.3, greyhole, greyholeMix = 0.25;
            // sig = (1 - delayMix) * sig + (delay * delayMix);
            sig = in;
            sig = BLowCut.ar(sig, 120);
            sig = BHiShelf.ar(sig, 400, rs:1.0, db: -12);
            verb = JPverbMono.ar(sig, size: 1.0, t60: 2.0);
            sig = (1 - verbMix) * sig + (verb * verbMix);
            greyhole = Greyhole.ar(sig, 0.3, feedback: 0.6);
            sig = (1 - greyholeMix) * sig + (greyhole * greyholeMix);
            sig = sig * 40.dbamp;
        });
        lowRateEnv = KFEnv.new([lowRateBegin, lowRateTarget], [rateTime], 5).play;
        highRateEnv = KFEnv.new([highRateBegin, highRateTarget], [rateTime], 5).play;
    }


    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|

            var buf = KF.buf[\flutterSpeak].choose;
            var numFrames = buf.numFrames;
            var currentPos = numFrames.rand;
            var durFrom = rrand(0.1, 0.2);
            var durTo = rrand(0.1, 0.2);
            var step = durFrom * KF.server.sampleRate;
            var count = 20;
            var len = dt.linlin(0.0, 1.0, step, step * count);
            var pos = (currentPos, currentPos+step..currentPos+len).mod(numFrames); 
            var time = pos[pos.size-1] - pos[0] / KF.server.sampleRate;

            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, buf,
                \dur, Pseg(durFrom, durTo, time),
                \attack, Pkey(\dur) * 0.5,
                \release, Pkey(\dur) * 1.5,
                \startPos, Pseq(pos),
                \legato, Pwhite(0.1, 0.2),
                \rate, Pexprand(lowRateEnv.value, highRateEnv.value),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pseg(Pwhite(0.0, 1.0), Pwhite(0.0, 1.0), time),
            );
        }
    }

    releaseAction {
        KF.freeBuffersSingleDir(\flutterSpeak);
    }
}
