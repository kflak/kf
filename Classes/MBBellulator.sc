MBBellulator : KFMBDeltaTrig {

    *new { arg db=0, speedlim=0.5, threshold=0.03, minAmp= -20, maxAmp=0, fadeTime=1;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime);
    }


    initBellulator {
        fx.filter(1, {|in|
            var sig = BHiPass.ar(in, 200);
            sig;
        })
    }


    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \fm,
                \octave, Pwrand((6..8), [0.5, 0.3, 0.2],  12),
                \degree, Prand([
                    Pwrand([[4, 2, 0], Rest()], [0.7, 0.3], inf),
                    Pwrand([[6, 2, 0], Rest()], [0.7, 0.3], inf),
                    Pwrand([[8, 2, 0], Rest()], [0.7, 0.3], inf),
                    Pwrand([[5, 3, 0], Rest()], [0.7, 0.3], inf),
                ], inf), 
                \freq, (Pkey(\octave) * 12 + Pkey(\degree)).midicps,
                \dur, Pseq((0.125!4)),
                \db, Pwhite(-34, -26),
                \pan, Pwhite(-1.0, 1.0),
                \modfreq, Pkey(\freq),
                \moddepth, Pwhite(10, 40),
                \attack, 0.1,
                \release, Pwhite(0.4, 0.8),
                \legato, 0.1,
            );
        }
    }
}
