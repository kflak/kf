MBGrowly : KFMBDeltaTrig {
    var <minAttack = 2;
    var <maxAttack = 0.1;
    var <minRelease = 0.2;
    var <maxRelease = 1;

    *new{|db= -6, speedlim=2, threshold=0.03, minAmp= -20, maxAmp=0, fadeTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBGrowly;
    }

    initMBGrowly{
        "MBGrowly initialized".postln;
        fx.filter (1, {|in|
            var sig, verbMix=0.1, verb, flanger, flangerMix=0.3;
            sig = BHiPass.ar(in, 80);
            verb = JPverbMono.ar(in, t60: 2);
            sig = (1 - verbMix) * sig + (verb * verbMix);
            flanger = Flanger.ar(sig, 0.5, feedback: 0.03, speed: 0.03);
            sig = (1 - flangerMix) * sig + (flanger * flangerMix);
            sig = Compander.ar(sig, sig, -6.dbamp, slopeAbove: 1/4);
        });
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var pmindex = rrand(0.5, 1.5);
            var attack = dt.linlin(0.0, 1.0, minAttack, maxAttack);
            var release = dt.linlin(0.0, 1.0, minRelease, maxRelease);

            fx[0] = Pmono(
                \pmFB,
                \dur, 0.1,
                \db, Pseg([-70, 0, -70], [attack, release]),
                \lag, Pkey(\dur),
                \freq, Pfunc{
                    var freqMul = rrand(1.0, 1.5);
                    var minFreq = rrand(40, 80);
                    var maxFreq = minFreq * freqMul;
                    var x = KF.mbData[id].x;
                    var freq = x.linexp(0.0, 1.0, minFreq, maxFreq);
                    freq
                },
                \modfreq, Pfunc{|ev| ev.use{ ~freq.() * rrand(0.4, 0.6)}},
                \modfeedback, Pfunc{ KF.mbData[id].y * 2 },
                \pmindex, Pfunc{
                    KF.mbData[id].x * pmindex
                },
            );
        }
    }
}
