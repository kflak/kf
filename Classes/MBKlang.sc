MBKlang : KFMBDeltaTrig {
    var <>octave = #[4, 5];
    var <>degrees = #[0];
    var <>scale;
    var <>dur = 0.25;
    var <>attackMul = 1;
    var <>releaseMul = 2;
    var <modDepth = 0.5;
    var <>root = 6;
    var <>spreadMin = 1.99;
    var <>spreadMax = 2.01;
    var <>durMin = 0.25;
    var <>durMax = 0.5;

    *new{|db=0, speedlim=0.5, threshold=0.1, minAmp= -50, maxAmp= -25, fadeTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBKlang;
    }

    initMBKlang{
        if(scale.isNil){ scale = Scale.major };
        fx.filter (1, {|in|
            var lag = \lag.kr(0.1);
            var sig;
            var verb, verbMix = 0.4;
            var mod;
            in = in * \inputgain.kr(10.dbamp);
            verb = JPverb.ar(in, t60: 3);
            sig = (1 - verbMix) * in + (verb * verbMix);
            sig = BHiPass.ar(in: sig, freq: LFNoise2.kr(1/4).range(40, 100));
            sig = BHiCut.ar(in: sig, freq: LFNoise2.kr(1/4).range(800, 2000));
            mod = LFNoise1.ar(LFNoise2.kr(1/4).range(1, 10)).range(1 - \modDepth.kr(modDepth), 1);
            sig = sig * mod; 
            // sig = sig * \amp.kr(0.dbamp);
        } )
    }

    modDepth_{|val| modDepth = val; fx.set(\modDepth, modDepth)}

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|
            var atk = dt.linlin(0.0, 1.0, 1, 2);
            var release = atk;
            var deg = degrees.choose;
            var numRepeats = dt.linlin(0.0, 1.0, 1, 5);

            fx[0] = Pbind(
                \instrument, \klang,
                \dur, Pwhite(durMin, durMax, numRepeats),
                \attack, Pkey(\dur) * attackMul * dt,
                \release, Pkey(\dur) * releaseMul * dt,
                \scale, scale,
                \root, root,
                \octave, octave,
                \degree, deg,
                \spread, Pwhite(spreadMin, spreadMax),
                \legato, 0.8,
                \pan, Pwhite(-1.0, 1.0),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
            );
        }
    }
}
