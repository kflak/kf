MBGremlins : MBShuffleGranulator {
    var <gain=40;
    var <gain2=40;
    var <reverbMix=0.2;
    var <reverbTime=2;


    *new{|db= 0, speedlim=0.3, threshold=0.02, minAmp= -6, maxAmp=0, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBGremlins;
    }

    initMBGremlins {
        KF.loadBuffersSingleDir("~/sc/mesi/audio/vocalImpro/");
        freeMBsImmediately = false;
        loop = true;
        buf = KF.buf['vocalImpro'];
        stride = 0.3;
        minDuration = 0.2;
        maxDuration = 1;
        attackMul = 0.0;
        releaseMul = 3;
        fx.filter (1, {|in|
            var lag = \lag.kr(0.1);
            var sig;
            var dMix, verbMix=\verbMix.kr(0.2);
            var comb, combMix=0.2;
            var dist, verb, locut, hishelf, out;
            sig = Compander.ar(in, in, -10.dbamp, slopeAbove: 1/8);
            sig = sig * \gain.kr(gain.dbamp);
            sig = sig.tanh;
            sig = BHiPass.ar(in: sig, freq: LFNoise2.kr(1/4).range(80, 400));
            sig = BHiShelf.ar(in: sig, freq: 500, db: -6);
            verb = JPverb.ar(in, t60: \verbTime.kr(2));
            sig = (1 - verbMix) * in + (verb * verbMix);
            sig = sig * \gain2.kr(gain2.dbamp );
            sig = sig.tanh;
            sig = sig * -20.dbamp;
            sig = BHiShelf.ar(in: sig, freq: 500, db: -9);
            sig = Limiter.ar(sig, -2.dbamp);
            sig = sig * \amp.kr(db.dbamp);
        });
    }

    gain_{|val| gain = val; fx.set(\gain, gain)}
    gain2_{|val| gain2 = val; fx.set(\gain2, gain2)}
    reverbMix_{|val| reverbMix = val; fx.set(\verbMix, reverbMix)}
    reverbTime_{|val| reverbTime = val; fx.set(\verbTime, reverbTime)}

    releaseAcion {
        KF.freeBuffersSingleDir('vocalImpro');
    }
}
