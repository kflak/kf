Cue {
    classvar <currentCue;

    var <func, <timeline;
    var <isPlaying=false;
    var events, clock;

    *new {|func|
        ^super.newCopyArgs(func).init;
    }

    init {
        events = ();
        timeline = [];
        clock = TempoClock.new();
    }

    play {
        timeline.pairsDo();
        isPlaying = true;
    }

    stop{
        clock.stop; 
    }

    free {
        clock.clear; 
    }
}
