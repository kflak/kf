MBStormulator : KFMBDeltaTrig {

    var <reverbTime=3, reverbMix=0.2;
    var <>buf;

    *new{|db=0, speedlim=0.5, threshold=0.035, minAmp= -12, maxAmp=6, fadeTime=10|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBStormulator;
    }

    initMBStormulator {
        KF.loadBuffersSingleDir("~/mySamples/coronaWhisper/");
        "MBStormulator initialized".postln;
        fx.filter(1, {|in|
            var lag = \lag.kr(0.1);
            var greyhole, greyholeMix = 0.3;
            var verb, verbMix = \reverbMix.kr(reverbMix);
            var sig = BHiShelf.ar(in, \hishelffreq.kr(400, lag), db: \hishelfdb.kr(-3, lag));
            sig = BHiPass.ar(sig, \locut.kr(120, lag));
            sig = Compander.ar(
                sig,
                sig, 
                slopeAbove: \ratio.kr(8, lag).reciprocal, 
                thresh: \thresh.kr(-12.dbamp, lag), 
                clampTime: \clampTime.kr(0.1, lag)
            );
            sig = Limiter.ar(sig, \limit.kr( -3.dbamp, lag));
            greyhole = Greyhole.ar(
                sig, 
                \delayTime.kr(0.3, lag),
                \damp.kr(0, lag),
                \size.kr(1, lag),
                \diff.kr(0.707, lag),
                \feedback.kr(0.6), 
                \modDepth.kr(0.1), 
                \modFreq.kr(2.0)
            );
            sig = (1 - greyholeMix) * sig + (greyhole * greyholeMix);
            verb = JPverbMono.ar(sig, t60: \reverbTime.kr(reverbTime));
            sig = (1 - verbMix) * sig + (verb * verbMix);
            sig = sig * \amp.kr(db.dbamp, lag);
            sig;
        });
    }
    
    reverbMix_{|val| reverbMix = val; fx.set(\reverbMix, reverbMix)}
    reverbTime_ {|val| reverbTime = val; fx.set(\reverbTime, reverbTime)}
    db_{|val| db = val; fx.set(\amp, db.dbamp)}


    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \grbufphasor, 
                \buf, Prand(KF.buf[\coronaWhisper]),
                \dur, dt.linlin(0.0, 1.0, 0.5, 2),
                \attack, Pkey(\dur),
                \release, Pkey(\dur),  
                \rate, Pwhite(0.5, 1),
                \rateDev, Pwhite(0.0, 0.05),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 1),
                \grainsize, Pwhite(0.03, 0.01),
                \grainfreq, Pkey(\grainsize).reciprocal,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
                \panDev, Pwhite(0.0, 1.0),
            );
        }
    }

    releaseAction {
        KF.freeBuffersSingleDir(\coronaWhisper);
    }
}
