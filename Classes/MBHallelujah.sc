MBHallelujah : KFMBDeltaTrig {

    *new { arg db=0, speedlim=0.5, threshold=0.02, minAmp= -36, maxAmp= -26, fadeTime=1;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBHallelujah;
    }

    initMBHallelujah {
        KF.loadBuffersSingleDir("~/mySamples/hallelujah/");
        fx.filter(1, {|in|
            var sig;
            var verb, verbMix = 0.3;
            var greyhole, greyholeMix = 0.2;
            verb = JPverbMono.ar(in, size: 1.0, t60: 3);
            sig = (1 - verbMix) * in + (verb * verbMix);
            greyhole = Greyhole.ar(sig, 0.3, feedback: 0.9);
            sig = (1 - greyholeMix) * sig + (greyhole * greyholeMix);
            sig;
        });
    }

    mbDeltaTrigFunction {
        var currentPos = 0;
        var initTime = thisThread.seconds;
        var convergeTime = 3 * 60;
        ^{|dt, minAmp, maxAmp, id|
            var buf = KF.buf[\hallelujah];
            var numFrames = buf[0].numFrames;
            var dur = 0.2;
            var step = dur * KF.server.sampleRate;
            var len = dt.linlin(0.0, 1.0, step, step * 10);
            var pos = (currentPos, currentPos+step..currentPos+len).mod(numFrames); 
            var currentTime = thisThread.seconds - initTime;
            currentPos = pos[pos.size-1].mod(numFrames); 

            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, Prand(buf),
                \dur, dur,
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * 2,
                \startPos, Pseq(pos),
                \legato, dur.linlin(0.01, 0.2, 0.2, 2),
                \rate, Pwhite(0.99, 1.01),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
            );
        }
    }
}
