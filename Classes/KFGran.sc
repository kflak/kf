KFGran {

    *ar { arg buf=0, rate=1, tFreq=20, posRate=1, startPos=0, rateModFreq=0, rateModDepth=0, tFreqModFreq=0, tFreqModDepth=0, posRateModFreq=0, posRateModDepth=0, pan=0, panDev=0, overlap=2, jitter=0, numChannels=2;

        var phasor;
        var bufFrames = BufFrames.ir(buf);
        var trigger;
        var tFreqMod = {
            SinOsc.ar(tFreqModFreq, Rand(0.0,2pi)) * tFreqModDepth;
        }; 
        var rateMod = {
            SinOsc.ar(rateModFreq, Rand(0.0,2pi)) * rateModDepth;
        }; 
        var posRateMod = {
            SinOsc.ar(posRateModFreq, Rand(0.0,2pi)) * posRateModDepth;
        };

        jitter = LFNoise2.kr(10).range(0, jitter);

        tFreq = tFreq + tFreqMod;
        posRate = posRate + posRateMod;
        rate = rate + rateMod;

        trigger = Impulse.ar(tFreq);

        phasor = Phasor.ar(
            rate: posRate * BufRateScale.kr(buf),
            start: startPos,
            end: bufFrames,
        );

        pan = pan + TRand.kr(0.0, panDev, trigger);

        ^GrainBuf.ar(
            numChannels: numChannels,
            trigger: trigger, 
            dur: tFreq.reciprocal * overlap,
            sndbuf: buf,
            rate: rate,
            pos: phasor / bufFrames + jitter,
            pan: pan,
        );
    }
}
