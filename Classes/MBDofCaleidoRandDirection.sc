MBDofCaleidoRandDirection : KFMBDeltaTrig {

    var<> maxRotation = 0.2;

    *new{|db= -6, speedlim=0.5, threshold=0.05, minAmp= 1, maxAmp=20, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime);
    }


    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var rotation = rrand(maxRotation * -1, maxRotation);
            DoF.setRotation(rotation);
        }
    }
}
