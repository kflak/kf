MBRainRingModGrain : KFMBDeltaTrig 
{
    *new { arg db=0, speedlim=0.5, threshold=0.03, minAmp= -20, maxAmp=0, fadeTime=1;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initRainRingModGrain;
    }

    initRainRingModGrain {
        KF.loadBuffersSingleDir("~/mySamples/rain/");
        fx.filter(1, {|in|
            var sig = BHiPass.ar(in, 200);
            var lag = \lag.kr(0.1);
            var mod = SinOsc.ar(\modfreq.kr(1, lag)) * \depth.kr(1, lag);
            var comb;
            var combMix = LFNoise2.kr(0.2).range(0.0001, 0.1);
            sig = sig * mod;
            sig = sig * 20.dbamp;
            comb = CombC.ar(sig, 0.2, LFNoise2.kr(0.1).range(100, 800).reciprocal);
            sig = (1 - combMix) * sig + (comb * combMix);
            sig;
        })
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(KF.buf[\rain]),
                \grainfreq, Pexprand(6, 80),
                \grainsize, Pkey(\grainfreq).reciprocal / 2,
                \posDev, Pwhite(0, 1000),
                \dur, dt.linlin(0.0, 1.0, 0.5, 2.0),
                \attack, Pkey(\dur) / 2,
                \release, Pkey(\dur),
                \legato, 0.8,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
            );
        }
    }

    releaseAction {
        // KF.freeBuffersSingleDir(\rain);
    }
}
