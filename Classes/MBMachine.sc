MBMachine {
    var <pool;
    var <maxSize;
    var <>waitTime;
    var <>scrambleClasses = true;
    var <>scrambleMBs = false;
    var <>mbs;
    var <>waitTimeCurve = -3;
    var containers;
    var count = 0;
    var task;

    *new{|pool, maxSize = 4, waitTime = #[5, 30]|
        ^super.newCopyArgs(pool, maxSize, waitTime);
    }

    play {
        if(mbs == nil){
            mbs = KF.mb;
        };
        if(scrambleMBs){
            mbs = mbs.scramble;
        };
        if(scrambleClasses){
            pool = pool.scramble;
        };

        containers = List.new(maxSize);

        // TODO: make a more flexible system than pairsDo
        mbs.pairsDo({|id1, id2|
            if(containers[count].isNil, {
                containers.add(pool[count].createInstance);
            });
            containers[count].play(id1);
            containers[count].play(id2);
            count = count + 1;
        });

        task = TaskProxy.new({
            loop {
                mbs.pairsDo({|id1, id2|
                    var wait;
                    containers.pop.free;
                    containers.addFirst(
                        pool[count.mod(pool.size)].createInstance
                    );
                    containers[0].play[id1];
                    containers[0].play[id2];
                    wait = rand(1.0).lincurve(0.0, 1.0, waitTime[0], waitTime[1], waitTimeCurve);
                    wait.wait;

                })
            }
        }).play;
    }

    free {|time|
        task.clear;
        containers.do(_.free(time));
        // pool.do(_.free(time));
    }
}
