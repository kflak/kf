MBBd : KFMBDeltaTrig{
    var <gain=20;
    var <reverbMix=0.5;
    var <reverbTime=3;

    *new{|db= -6, speedlim=0.5, threshold=0.05, minAmp= -40, maxAmp=0, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBBd;
    }

    initMBBd {
        KF.loadBuffersSingleDir("~/mySamples/bd/");
        fx.filter (1, {|in|
            var lag = \lag.kr(0.1);
            var sig;
            var verb, verbMix=\verbMix.kr(reverbMix);
            var t60=\t60.kr(reverbTime);
            sig = in * \inputgain.kr(gain.dbamp);
            sig = sig.tanh;
            sig = BHiPass.ar(in: sig, freq: 60);
            sig = BHiShelf.ar(in: sig, freq: 200, db: -6);

            verb = JPverbMono.ar(sig, t60: t60);
            sig = (1 - verbMix) * sig + (verb * verbMix);
            sig = sig * 15.dbamp;
            sig = sig * \amp.kr(0.dbamp);
        })
    }

    gain_{|val| gain = val; fx.set(\inputgain, gain.dbamp)}
    reverbMix_{|val| reverbMix = val; fx.set(\verbMix, reverbMix)}
    reverbTime_{|val| reverbTime = val; fx.set(\t60, reverbTime)}

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, Prand(KF.buf[\bd]),
                \dur, 1,
                \release, Pkey(\dur) * 2,  
                \loop, 0,
                \legato, 0.1,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
            );
        }
    }

    releaseAction {
        KF.freeBuffersSingleDir(\bd);
    }
}

