MBBreathulator :  KFMBDeltaTrig {

    var <reverbTime = 1.0, <reverbMix = 0.3;

    *new{|db=0, speedlim=0.5, threshold=0.1, minAmp= -40, maxAmp=0, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBBreathulator;
    }

    initMBBreathulator {
        KF.loadBuffersSingleDir("~/mySamples/breath/");
        "MBBreathulator initialized".postln;
        fx.filter(1, {|in|
            var lag = \lag.kr(0.1);
            var verb, verbMix = \reverbMix.kr(reverbMix);
            var sig = in;
            // var sig = BHiShelf.ar(in, \hishelffreq.kr(400, lag), db: \hishelfdb.kr(6, lag));
            sig = BHiPass.ar(sig, \locut.kr(100, lag));
            verb = JPverbMono.ar(sig, t60: \reverbTime.kr(reverbTime));
            sig = (1 - verbMix) * sig + (verb * verbMix);
            sig = sig * 40.dbamp;
            sig;
        });
    }

    reverbMix_{|val| reverbMix = val; fx.set(\reverbMix, reverbMix)}
    reverbTime_ {|val| reverbTime = val; fx.set(\reverbTime, reverbTime)}


    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(KF.buf[\breath]),
                \dur, dt.linlin(0.0, 1.0, 1.0, 4.0),
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * 2,  
                \rate, 1,
                \rateDev, Pwhite(0.0, 0.5),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 0.5),
                \grainfreq, Pwhite(2, 40),
                \grainsize, Pkey(\grainfreq).reciprocal * Pwhite(1.0, 4.0),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \panDev, Pwhite(0.0, 1.0),
            );
        }
    }

    releaseAction {
        KF.freeBuffersSingleDir(\breath);
    }
}
