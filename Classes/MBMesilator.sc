MBMesilator : KFMBDeltaTrig {

    var <>grainSize=0.1;
    var <>grainSizeDeviation = 0.01;
    var <>overlap = 8;
    var <>timeStretch = 8;
    var <>rate = 1;
    var <>rateDeviation = 0;

    *new{|db= -6, speedlim=0.5, threshold=0.1, minAmp= -6, maxAmp=0, fadeTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBTreePerculator;
    }

    initMBTreePerculator {
        KF.loadBuffersSingleDir("~/mySamples/mesi/");
        fx.filter (1,  {|in|
            var sig;
            var dMix, verbMix=0.3;
            var dist, verb, locut, hishelf, out;
            in = in * \inputgain.kr(20.dbamp);
            sig = JPverb.ar(in, t60: 2);
            sig = (1 - verbMix) * in + (sig * verbMix);
            sig = BHiPass.ar(in: sig, freq:60.0);
            out = sig * \amp.kr(db.dbamp);
        });
    }


    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var buf = KF.buf[\mesi].choose;
            var bufDur = buf.duration;
            var db = dt.linlin(0.0, 1.0, minAmp, maxAmp);
            var totalDur = min( dt.linlin(0.0, 1.0, 2, 8), bufDur );
            var attack = totalDur * 0.5;
            var release = totalDur * 0.5;

            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, buf,
                \rate, max( rate + bilinrand(rateDeviation), 0.0001),
                \startPos, Pseg([0, bufDur], bufDur * timeStretch), 
                \dur, Pfunc{ max( grainSize + bilinrand(grainSizeDeviation), 0.005 )},
                \attack, attack,
                \release, release,
                \legato, overlap,
                \db, Pseg([-70, db, -70], [attack, release]),
                \pan, Pseg([rrand(-1.0, 1.0), rrand(-1.0, 1.0)], totalDur),
            );
        }
    }
}

