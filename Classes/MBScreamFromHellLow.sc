MBScreamsFromHellLow : MBShuffleGranulator {

    *new{|db= -10, speedlim=0.3, threshold=0.02, minAmp= -6, maxAmp=0, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBScreamsFromHellLow;
    }

    initMBScreamsFromHellLow {
        this.prAddFx;
        KF.loadBuffersSingleDir("~/mySamples/screamsFromHellLow/");
        freeMBsImmediately = false;
        loop = true;
        buf = KF.buf[\screamsFromHellLow];
        stride = 0.3;
    }

    prAddFx {
        fx.filter (1, {|in|
            var sig;
            var dMix, verbMix=0.3;
            var dist, verb, locut, hishelf;
            in = in * \inputgain.kr(20.dbamp);
            verb = JPverb.ar(in, t60: 2);
            sig = (1 - verbMix) * in + (verb * verbMix);
            sig = sig.tanh;
            sig = Compander.ar(sig, sig, -10.dbamp, slopeAbove: 1/8);
            sig = BHiPass.ar(in: sig, freq: LFNoise2.kr(1/4).range(80, 400));
            sig = BHiShelf.ar(in: sig, freq: 500, db: -3);
            sig = sig * \amp.kr(db.dbamp);
        });
    }

    releaseAcion {
        KF.freeBuffersSingleDir(\screamsFromHellLow);
    }
}
