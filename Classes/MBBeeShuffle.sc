MBBeeShuffle : MBShuffleGranulator {

    var <task;
    var <cc = true;

    *new{|db= -10, speedlim=0.3, threshold=0.02, minAmp= -6, maxAmp=0, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBMesiShuffle;
    }

    initMBMesiShuffle {
        KF.loadBuffersSingleDir("~/mySamples/bees/");
        freeMBsImmediately = false;
        loop = true;
        buf = KF.buf[\bees];
        stride = 0.3;
        minDuration = 0.2;
        maxDuration = 2;
        attackMul = 0.7;
        releaseMul = 5;
        fx.filter (1, {|in|
            var lag = \lag.kr(0.1);
            var sig;
            var dMix, verbMix=0.1;
            var dist, verb, locut, hishelf, out;
            sig = in * \inputgain.kr(20.dbamp);
            verb = JPverb.ar(in, t60: 2);
            sig = (1 - verbMix) * sig + (verb * verbMix);
            sig = Compander.ar(sig, sig, -10.dbamp, slopeAbove: 1/8);
            sig = BHiPass.ar(in: sig, freq: LFNoise2.kr(1/4).range(80, 400));
            sig = BHiShelf.ar(in: sig, freq: 500, db: -3);
            sig = sig * \amp.kr(db.dbamp);
        });
    }

    cc_{|val| cc = val; 
        if(cc){
            this.prCreateTask;
        }{
            task.clear;
        }
    }

    prCreateTask {
        task = TaskProxy.new;
        task.play;
        task.source = {
            loop{
                minPitch = KF.mbData[15].x.linlin(0.0, 1.0, -24.0, 24.0);
                maxPitch = KF.mbData[16].x.linlin(0.0, 1.0, -24.0, 24.0);
                0.1.wait;
            }
        }
    }

    releaseAcion {
        KF.freeBuffersSingleDir(\bees);
        task.clear;
    }
}
