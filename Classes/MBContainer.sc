// Container for an MBDeltaTrig class.
MBContainer {


    var <class;
    var <arglist;
    var <instance;
    var <db, <fadeTime, <threshold, <speedlim, <maxAmp, <minAmp;

    *new{|class, arglist|
        ^super.newCopyArgs(class, arglist);
    }

    db_{|val| instance.db = val; db = val; }
    speedlim_{|val| instance.speedlim_(val); speedlim = val}
    threshold_{|val| instance.threshold_(val); threshold = val}
    minAmp_{|val| instance.minAmp_(val); minAmp = val}
    maxAmp_{|val| instance.maxAmp_(val); maxAmp = val}
    fadeTime_{|val| instance.fadeTime_(val); maxAmp = val}

    createInstance {
        instance = class.new;
        // set values of the instance
        instance.set(*arglist);
        // copy over the current values of the instance to this.
        db = instance.db;
        speedlim = instance.speedlim;
        fadeTime = instance.fadeTime;
        threshold = instance.threshold;
        maxAmp = instance.maxAmp;
        minAmp = instance.minAmp;
    }

    set{|... args|
        instance.set(*args);
    }

    play{|mbID|
        if(mbID.notNil, {
            instance.play(mbID);
        },{
            instance.play;
        })
    }

    free{|releaseTime|
        instance.free(releaseTime);
    }

    freeMB{|mbID|
        instance.freeMB(mbID);
    }

    release{|releaseTime|
        instance.release(releaseTime);
    }
}
