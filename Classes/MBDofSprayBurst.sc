MBDofSprayBurst : KFMBDeltaTrig {


    *new{|db= -6, speedlim=0.5, threshold=0.05, minAmp= 1, maxAmp=20, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initSprayBurst;
    }


    initSprayBurst {
        DoF.scene("Spray");
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            DoF.sprayBurst(dt.linlin(0.0, 1.0, 100, 1000));
        }
    }
}
