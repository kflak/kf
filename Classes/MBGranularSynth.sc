MBGranularSynth : KFMBDeltaTrig {

    var <>buf;
    var <>minDur=0.5, <>maxDur=2;
    var <>attack=0.1, <>release=0.5;
    var <>rate=1, <>rateDev=0;
    var <>pos=0, <>posDev=0;
    var <>playbackRate=1;
    var <>grainsize=0.05;
    var <>grainfreq=20;
    var <>pan=0, <>panDev=0;

    var algorithm = \grbufphasor;
    var grainFunction;

    *new{|db=0, speedlim=0.5, threshold=0.1, minAmp= -40, maxAmp=0, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime);
    }

    // initMBG {
        // grainFunction = Dictionary.new;
        // this.prCreateFunctions;
    // }

    mbDeltaTrigFunction {
        // ^grainFunction[algorithm];
        ^{|dt, minAmp, maxAmp, id|
            if(fx.isPlaying && buf.notNil){
                var b;
                if(buf.class == Collection){
                    b = buf.choose; 
                }{
                    b = buf;
                };

                fx[0] = Pbind(
                    \instrument, \grbufphasor,
                    \buf, b,
                    \dur, dt.linlin(0.0, 1.0, minDur, maxDur),
                    \attack, attack,
                    \release, release,
                    \rate, rate,
                    \rateDev, Pwhite(0, rateDev),
                    \posDev, Pwhite(0, posDev),
                    \playbackRate, playbackRate,
                    \grainsize, grainsize,
                    \grainfreq, grainfreq,
                    \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                    \pan, pan,
                    \panDev, Pwhite(0, panDev),
                );
            }
        }
    }

}
