MBVemKan : KFMBDeltaTrig {

    var current = 0;

    *new { arg db=0, speedlim=0.5, threshold=0.02, minAmp= -36, maxAmp= -26, fadeTime=1;
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBVemKan;
    }

    initMBVemKan {
        KF.loadBuffersSingleDir("~/mySamples/vemKanChopped/");
        globalSpeedlim = 1.5;
        fx.filter(1, {|in|
            var sig = in;
            var verb, verbMix = 0.3;
            // var greyhole, greyholeMix = 0.2;
            verb = JPverbMono.ar(in, size: 1.0, t60: 2);
            sig = (1 - verbMix) * in + (verb * verbMix);
            // greyhole = Greyhole.ar(sig, 0.3, feedback: 0.7);
            // sig = (1 - greyholeMix) * sig + (greyhole * greyholeMix);
            sig;
        });
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            if(isPlaying.not){
                fx[0] = Pbind(
                    \instrument, \grbufphasor,
                    \buf, KF.buf[\vemKanChopped][current],
                    \dur, Pfunc{|ev| ev.buf.duration * 0.6},
                    \attack, Pkey(\dur) * 0.2,
                    \release, Pkey(\dur) * 0.2,
                    \rate, 1,
                    \posDev, Pwhite(0, 0.01),
                    \playbackRate, 1,
                    \grainsize, Pwhite(0.01, 0.8),
                    \grainfreq, Pwhite(4, 20),
                    \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                    \pan, Pwhite(-1.0, 1.0, 1),
                    \panDev, Pwhite(0.0, 1.0),
                );
                current = (current + 1) % KF.buf[\vemKanChopped].size;
                isPlaying = true;
                SystemClock.sched(globalSpeedlim, {
                    isPlaying = false;
                });
            }
        }
    }


    releaseAction {
        // KF.freeBuffersSingleDir(\vemKanChopped);
    }
}
