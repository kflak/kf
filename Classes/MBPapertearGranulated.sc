MBPapertearGranulated : KFMBDeltaTrig {

    *new{|db=0, speedlim=0.5, threshold=0.07, minAmp= -9, maxAmp=3, fadeTime=3|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBEarthulator;
    }

    initMBEarthulator{
        KF.loadBuffersSingleDir("~/mySamples/paperTear/");
        fx.filter (1, {|in| 
            var sig, verb, verbMix = 0.01;
            in = in * \inputgain.kr(0.dbamp);
            sig = Compander.ar(in, in, -4.dbamp, slopeAbove: 1/8) * \compGain.kr(9.dbamp);
            sig = BHiPass.ar(sig, 80);
            // sig = BHiShelf.ar(sig, 500, db: -3);
            verb = JPverbMono.ar(sig, size: 1, t60: 2);
            sig = (1 - verbMix) * sig + (verb * verbMix);
            sig = sig * \amp.kr(db.dbamp);
        });
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(KF.buf[\paperTear]),
                \dur, dt.linlin(0.0, 1.0, 0.5, 2),
                \attack, Pkey(\dur) * 0.2,
                \release, Pkey(\dur) * 2,  
                \rate, Prand([0.25, 1], inf),
                \rateDev, Pwhite(0.0, 0.5),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 2),
                \grainsize, Pwhite(0.01, 0.5),
                \grainfreq, Pwhite(2, 40),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \panDev, Pwhite(0.0, 1.0),
            );
        }
    }

    releaseAction {
        KF.freeBuffersSingleDir(\paperTear);
    }
}
