KFSynthDefs { 
    var numChannels;
    var synthDefAsrWithPan;
    var synthDefAsrWithoutPan;
    var synthDefPercWithPan;
    var synthDefPercWithoutPan;
    var server;

    *new{|numChannels=2|
        ^super.newCopyArgs(numChannels).init;
    }

    init{
        server = server ? Server.default;
        if(server.serverRunning){
            this.prMakeSynthDefs;
        }{
            "Server not running".postln;
        }
    }

    prMakeSynthDefs{
        synthDefAsrWithPan = (
            crackle: {|sig|
                var freq = \freq.kr(200);
                var trig = Dust.ar(freq);
                var env = EnvGen.kr(Env([0, 1, 0], [0.001, freq.reciprocal]), trig);
                sig = PinkNoise.ar;
                sig = sig * env;
                sig = BLowPass.ar(sig, TRand.kr(200, 20000, trig));
                sig = LeakDC.ar(sig);
                // sig = sig * 10.dbamp;
            },
            sineGrain: {|sig|
                var trig = Impulse.ar(\tFreq.kr(10));
                var freq = \freq.kr(440);
                sig = SinGrain.ar(trig, \grainsize.kr(0.1), freq);
                sig = sig * AmpCompA.kr(freq);
            },
            fftRandBins: {|sig|
                var in, numFrames=2048, chain, a, minModFreq, maxModFreq;
                minModFreq = \minModFreq.kr(0.1);
                maxModFreq = \maxModFreq.kr(20);
                a = Array.fill(100, { 
                    SinOsc.kr(ExpRand.new(minModFreq, maxModFreq).range(0.0, 1.0))
                }).resamp1(numFrames);
                in =  In.ar(\in.kr(numChannels));
                chain = FFT(LocalBuf(numFrames), in);
                chain = chain.pvcalc(numFrames, {|mags, phases|
                    mags * a;
                });
                sig = IFFT(chain) * \amp.kr(1.0);
            },

            pink: {|sig|
                var lag = \lag.kr(0.1);
                var lfnoise = LFNoise1.ar(\lfnoiseFreq.kr(10)); 
                sig = PinkNoise.ar(); 
                sig = BHiPass.ar(sig, \locut.kr(20, lag));
                sig = BLowPass.ar(sig, \hicut.kr(20000, lag));
                sig = sig * (1 - lfnoise.range(0.0, \modDepth.kr(1.0)));
            },

            playbuf: {|sig|
                var buf = \buf.kr(0);
                sig = PlayBuf.ar(
                    1, 
                    buf, 
                    rate: BufRateScale.kr(buf) * \rate.kr(1), 
                    startPos: \startPos.kr(0),
                    loop: \loop.kr(1)
                );
            },

            membrane: {|sig|
                var excitation = EnvGen.ar(Env.perc(0.01, 1, 1), doneAction: 0) * PinkNoise.ar(0.4);
                sig = MembraneCircle.ar(
                    excitation,
                    \tension.kr(0.05),
                    \loss.kr(0.99999)
                );
            },

            klang: {|sig|
                var freq = \freq.kr(440);
                var freqs = Array.geom(8, freq, \spread.ir(2));
                var amps = Array.geom(8, 1, \decay.ir(0.3));
                sig = Klang.ar(`[freqs, amps, nil]);
                sig = sig * AmpCompA.kr(freq);
            },

            sine: {|sig|
                var freq = \freq.kr(440, 0.1);
                sig = SinOsc.ar(freq);
                sig = sig * AmpCompA.kr(freq);
            },

            analogBassDrum: {|sig|
                var gate = \gate.kr(1);
                sig = AnalogBassDrum.ar(
                    gate,
                    \infsustain.kr(0.0),
                    \accent.kr(0.5),
                    \freq.kr(50),
                    \tone.kr(0.5)
                );
            },

            analogSnareDrum: {|sig|
                var gate = \gate.kr(1);
                sig = AnalogSnareDrum.ar(
                    gate,
                    \infsustain.kr(0.0),
                    \accent.kr(0.5),
                    \freq.kr(200),
                    \tone.kr(0.5),
                    \decay.kr(0.5),
                    \snappy.kr(0.5)
                );
            },

            pmFB: {|sig|
                var lag = \lag.kr(0.1);
                var freq = \freq.kr(440, lag);
                var modenv = EnvGen.kr(Env.asr(\modattack.kr(0.1), 1.0, \modrelease.kr(0.1)), gate: \gate.kr(1));
                var mod = modenv * \pmindex.kr(0, lag);
                sig = PMOscFB.ar(
                    freq,
                    \modfreq.kr(440, lag),
                    mod,
                    \modfeedback.kr(0.0, lag)
                );
                sig = sig * AmpCompA.kr(freq);
            },

            vosc: {|sig|
                var freq = \freq.kr(440);
                sig  = VOsc.ar(
                    \bufpos.kr(0), 
                    freq
                );
                sig = sig * AmpCompA.kr(freq);
            },

            rec: {|sig|
                var in = In.ar(\in.kr(numChannels));
                var id = \id.ir(0);
                var rec = \rec.ar(1);
                var buf = \buf.kr(0);
                var lag = \lag.kr(0.1);
                var frames = BufFrames.kr(buf);
                var phase = Phasor.ar(0, BufRateScale.kr(buf), 0, frames);
                var stopTrig = (rec <= 0);
                BufWr.ar(in, buf, phase);
                SendReply.ar(Impulse.ar(\trigFreq.kr(20)), '/recPos', phase, id);
                SendReply.ar(stopTrig, '/recEnded', phase, id);
                sig = in * \amp.kr(1, lag);
            },

            panner: {|sig|
                sig = In.ar(\in.kr(numChannels), 1);
            },

        );

        synthDefAsrWithoutPan = (
            playbufStereo: {|sig|
                var buf = \buf.kr(0);
                sig = PlayBuf.ar(
                    2, 
                    buf, 
                    rate: BufRateScale.kr(buf) * \rate.kr(1), 
                    startPos: \startPos.kr(0),
                    loop: \loop.kr(1)
                );
            },

            pvEq: {|sig|
                var in, mix, lag, numFrames, magnitudes, chain, tobin;
                numFrames = 2048;
                tobin = 256;
                lag = \lag.kr(0.1);
                mix = \mix.kr(1, lag);
                in = In.ar(\in.kr((KF.numSpeakers + KF.numSubs), KF.numSpeakers)); 
                chain = FFT(LocalBuf(numFrames ! KF.numSpeakers), in);
                magnitudes = \magnitudes.kr(1 ! tobin, lag);
                chain = chain.collect{|i|
                    i.pvcalc(numFrames, {|mags|
                        mags * magnitudes;
                    }, tobin: tobin);
                };
                sig = IFFT(chain);
            },

            resynth: {|sig|
                
                var freq = \freq.kr(440, \lag.kr(0.1));

                // based on a single trumpet note in concert Bb.
                var baseFreqs = \baseFreqs.kr(#[
                    233, 480, 717, 957, 1190, 1423, 1652, 1889, 2119, 2362, 2835
                ]);

                // calculate the ratios
                var freqs = baseFreqs.size.collect({|i|
                    freq * (baseFreqs[i] / baseFreqs[0]);
                });

                var freqMod = freqs.collect({
                    SinOsc.kr(rrand(0.01, 0.1)).range(0.99, 1.01)
                });

                var amps = \dbs.kr(#[
                    -33, -29, -30, -32, -32, -37, -37, -44, -52, -49, -60
                ]).dbamp;

                var ampMod = amps.collect({|i, idx|
                    SinOsc.kr(rrand(0.01, 0.1)).range(0.5, 1.0); 
                });

                var fundamental, overtones;
                sig = SinOsc.ar(freqs * freqMod);
                sig = sig * amps * ampMod * 6.dbamp * AmpCompA.kr(freq);
                fundamental = sig[0];
                overtones = sig[1..];
                overtones = overtones.scramble;
                sig = fundamental + overtones;
                sig = SplayAz.ar(numChannels, sig);
            },
            lfnoise: {|sig|
                var lfnoise = LFNoise1.ar(\freq.kr(10)) ! numChannels;
                sig = In.ar(\in.kr(numChannels), numChannels);
                sig = sig * lfnoise;
            },

            gain: {|sig|
                sig = In.ar(\in.kr(numChannels), numChannels);
                sig = sig * \gain.kr(1);
            },


            tanh: {|sig|
                sig = In.ar(\in.kr(BBS.numSpeakers), BBS.numSpeakers);
                sig = sig * \gain.kr(1);
                sig = sig.tanh;
            },

            dist: {|sig|
                sig = In.ar(\in.kr(numChannels), numChannels);
                sig = sig * \gain.kr(1);
                sig = sig.distort;
            },

            eq: {|sig|
                var lag = \lag.kr(0.1);
                sig = In.ar(\in.kr(numChannels), numChannels);
                sig = BLowShelf.ar(sig, \loshelffreq.kr(200, lag), db: \loshelfdb.kr(0, lag));
                sig = BHiShelf.ar(sig, \hishelffreq.kr(1500, lag), db: \hishelfdb.kr(0, lag));
                sig = BHiPass.ar(sig, \locut.kr(20, lag));
                sig = BLowPass.ar(sig, \hicut.kr(20000, lag));
                sig = BPeakEQ.ar(sig, \peakfreq.kr(600, lag), \peakrq.kr(1, lag), \peakdb.kr(0, lag));
            },

            greyhole: {|sig|
                var source, mix, lag;
                lag = \lag.kr(0.1);
                mix = \mix.kr(0.3, lag);
                source = In.ar(\in.kr(numChannels), numChannels);
                source = Splay.ar(source);
                sig = Greyhole.ar(
                    source, 
                    \delayTime.kr(1, lag),
                    \damp.kr(0, lag),
                    \size.kr(1, lag),
                    \diff.kr(0.707, lag),
                    \feedback.kr(0.9), 
                    \modDepth.kr(0.1), 
                    \modFreq.kr(2.0)
                );
                sig = Splay.ar(sig, spread: 0.5);
                sig = (1 - mix) * source + (sig * mix);
            },

            jpverb: {|sig|
                var source, mix, lag;
                lag = \lag.kr(0.1);
                mix = \mix.kr(0.3, lag);
                source = In.ar(\in.kr(numChannels), numChannels);
                source = Mix.ar(source);
                sig = JPverb.ar(
                    source,
                    \revtime.kr(1), 
                    \damp.kr(0), 
                    \size.kr(1), 
                    \early.kr(0.707)
                );
                sig = SplayAz.ar(KF.numSpeakers, sig, spread: 0.5, width: KF.numSpeakers);
                sig = (1 - mix) * source + (sig * mix);
            },

            route: {|sig|
                sig = In.ar(\in.kr(numChannels), numChannels);
            },

            dopplerPitchShift: {|sig|
                var lag = \lag.kr(0.1);
                var mix = \mix.kr(1.0, lag);
                var source = In.ar(\in.kr(numChannels), numChannels);
                // source = Mix.ar(source);
                sig = DopplerPitchShift.ar(source, \ratio.kr(1));
                sig = (1 - mix) * source + (sig * mix);
            },

            flanger: {|sig|
                var in, maxdelay, maxrate, dsig, mixed, mix, local, lag; 
                lag = \lag.kr(0.1);
                mix = \mix.kr(1, lag);
                maxdelay = 0.013; 
                maxrate = 10.0; 
                in = In.ar(\in.kr((numChannels), numChannels)); 
                local = LocalIn.ar(numChannels); 
                dsig = AllpassL.ar(
                    in + (local * \feedback.kr(0.0, lag)), 
                    maxdelay * 2, 
                    LFPar.kr( 
                        \rate.kr(0.06, lag) * maxrate, 
                        0, 
                        \depth.kr(0.08, lag) * maxdelay, 
                        \delay.kr(0.1, lag) * maxdelay
                    ), 
                    \decay.kr(0.0, lag)
                ); 
                mixed = in + dsig;
                LocalOut.ar(mixed); 
                sig = (1 - mix) * in + (mixed * mix);
            },

            ringMod: {|sig|
                var source, mix, mod, lag;
                lag = \lag.kr(0.1);
                mix = \mix.kr(1, lag);
                mod = SinOsc.ar(\modfreq.kr(1, lag)) * \depth.kr(1, lag);
                source = In.ar(\in.kr(numChannels), numChannels);
                sig = source * mod;
                sig = (1 - mix) * source + (sig * mix);
            },

            comb: {|sig|
                var source, mix, lag;
                lag = \lag.kr(0.1);
                source = In.ar(\in.kr(numChannels), numChannels);
                sig = CombC.ar(
                    source, 
                    \maxDelay.ir(1),
                    \delay.kr(0.1, lag),
                    \decay.kr(1, lag)
                );
                mix = \mix.kr(1, lag);
                sig = (1 - mix) * source + (sig * mix);
            },

            compressor: {|sig|
                var lag = \lag.kr(0.1);
                sig = In.ar(\in.kr(numChannels), numChannels);
                sig = Compander.ar(
                    sig,
                    sig, 
                    slopeAbove: \ratio.kr(2, lag).reciprocal, 
                    thresh: \thresh.kr(0.7, lag), 
                    clampTime: \clampTime.kr(0.1, lag)
                );
            },
            noiseMod: {|sig|
                var lag = \lag.kr(0.1);
                var mod = LFNoise1.ar(\modfreq.kr(1, lag)) * \depth.kr(1, lag);
                var source = In.ar(\in.kr(numChannels), numChannels);
                var mix = \mix.kr(1, lag);
                sig = source * mod;
                sig = (1 - mix) * source + (sig * mix);
            },

            limiter: {|sig|
                var lag = \lag.kr(0.1);
                sig = In.ar(\in.kr(numChannels), numChannels);
                sig = Limiter.ar(sig, \limit.kr(0.9, lag));
            },

            granulator: {|sig|
                var buf = \buf.kr(0);
                var tFreq, rate, posRate;
                var phasor;
                var bufFrames = BufFrames.ir(buf); 
                var trigger;
                var pan, panDev;
                var lag = \lag.kr(0.05);
                var tFreqMod = { 
                    SinOsc.ar(\tFreqModFreq.kr(0), Rand(0.0,2pi)) * \tFreqModDepth.kr(0);
                };
                var rateMod = { 
                    SinOsc.ar(\rateModFreq.kr(0), Rand(0.0,2pi)) * \rateModDepth.kr(0);
                };
                var posRateMod = { 
                    SinOsc.ar(\posRateModFreq.kr(0), Rand(0.0,2pi)) * \posRateModDepth.kr(0);
                };

                var jitter = LFNoise2.kr(10).range(0, \jitter.kr(0));

                tFreq = \tFreq.kr(20, lag) + tFreqMod;
                posRate = \posRate.kr(1) + posRateMod;
                rate = \rate.kr(1) + rateMod;

                trigger = Impulse.ar(tFreq);

                phasor = Phasor.ar(
                    rate: posRate * BufRateScale.kr(buf), 
                    start: \startPos.kr(0.0),
                    end: bufFrames, 
                );

                pan = \pan.kr(0);
                panDev = \panDev.kr(0);
                pan = pan + TRand.kr(0.0, panDev, trigger);

                sig = GrainBuf.ar(
                    numChannels: numChannels,
                    trigger: trigger, 
                    dur: tFreq.reciprocal * \overlap.kr(2), 
                    sndbuf: buf, 
                    rate: rate, 
                    pos: phasor / bufFrames + jitter, 
                    pan: pan,
                );
            },

            grbufphasor: {|sig|

                var pos, posDev, buf, duration, trig, rate, rateDev, startPos, endPos, pan, panDev, playbackRate;
                buf = \buf.kr(0);
                playbackRate = \playbackRate.kr(1);
                startPos = \startPos.kr(0);
                endPos = \endPos.kr(1);
                duration = (endPos - startPos).abs * (BufFrames.kr(buf) / SampleRate.ir());
                trig = Impulse.kr(\grainfreq.kr(10));
                posDev = \posDev.kr(0);
                pos = Line.kr(startPos, endPos, (duration * playbackRate.reciprocal)); 
                pos = (pos + TRand.kr(0.0, posDev, trig)).mod(endPos);
                rate = \rate.kr(1);
                rateDev =  \rateDev.kr(0);
                rate = rate + TRand.kr(0.0, rateDev, trig);
                pan = \pan.kr(0);
                panDev = \panDev.kr(0);
                pan = pan + TRand.kr(0.0, panDev, trig);
                // pan = pan.mod(1.0);
                sig = GrainBuf.ar(numChannels, trig, \grainsize.kr(0.2), buf, rate, pos, pan: pan);
            },

            additiveBrass: {|sig|
                var freq = \freq.kr(440);
                // based on a single trumpet note in concert Bb.
                var baseFreqs = [
                    233, 480, 717, 957, 1190, 1423, 1652, 1889, 2119, 2362, 2835
                ];

                // calculate the ratios
                var freqs = baseFreqs.size.collect({|i|
                    freq * (baseFreqs[i] / baseFreqs[0]);
                });

                var freqMod = freqs.collect({
                    SinOsc.kr(rrand(0.01, 0.1)).range(0.99, 1.01)
                });

                var amps = [
                    -33, -29, -30, -32, -32, -37, -37, -44, -52, -49, -60
                ].dbamp;

                var ampMod = amps.collect({|i, idx|
                    SinOsc.kr(rrand(0.01, 0.1)).range(0.5, 1.0); 
                });

                var env = Env.asr( 
                    \attack.kr(0.01), 1, \release.kr(0.01)
                ).kr(
                    gate: \gate.kr(1), doneAction: \da.kr(2)
                );

                var amp = amps * ampMod * 6.dbamp * \amp.kr(0.dbamp) * AmpCompA.kr(freq) * env;

                var fundamental, overtones;

                sig = SinOsc.ar(freqs * freqMod) * amp;

                fundamental = sig[0];
                overtones = sig[1..];
                overtones = overtones.scramble;
                sig = fundamental + overtones;
                sig = SplayAz.ar(KF.numSpeakers, sig);
            }
        );

        synthDefPercWithPan = (
            // thanks, Nathan Ho!
            snare: {|sig|
                sig = SinOsc.ar(
                    230
                    * (1 + (0.3 * Env.perc(0.001, 0.01, curve: -8).ar)) 
                    * [1, 2.3, 5.6, 8.9]);
                    sig = sig * ([1] ++ Env.perc(0, [0.1, 0.03, 0.15]).ar);
                    sig = sig * [0, -15, -8, -15].dbamp;
                    sig = sig.sum;
                    sig = sig * (1 + (5 * Env.perc(0, 0.01).ar));
                    sig = sig + (BPF.ar(Hasher.ar(Sweep.ar), 2320, 0.3) * Env.perc(0.05, 0.1).ar * -1.dbamp);
                    sig = (sig * 0.dbamp).tanh;
                    sig = sig * -10.dbamp;
                },

                kick: {|sig|
                    var freq = XLine.kr(Rand(90, 110), Rand(20, 40), 0.16);
                    var transient = PinkNoise.ar();
                    transient = transient * EnvGen.kr(Env.perc(0.0, 0.02)) * \transientLevel.kr(1);
                    sig = SinOsc.ar(freq, 1.047);
                    sig = sig + transient;
                    sig = sig + SinOsc.ar(freq + \detune.kr(0.01));
                    sig = sig * \distortion.kr(2).dbamp;
                    sig = sig.tanh;
                    sig = sig + (SinOsc.ar([50, 51, 54]) * -6.dbamp);
                },

                grain: {|sig|
                    var buf = \buf.kr(0);
                    sig = PlayBuf.ar(1, buf, rate: BufRateScale.kr(buf) * \rate.kr(1), startPos: \startPos.kr(0), loop: \loop.kr(1));
                },

                playbufPerc: {|sig|
                    var buf = \buf.kr(0);
                    sig = PlayBuf.ar(
                        1, 
                        buf, 
                        rate: BufRateScale.kr(buf) * \rate.kr(1), 
                        startPos: \startPos.kr(0),
                        loop: \loop.kr(0)
                    );
                },

                reso: {|sig|
                    var excitor = WhiteNoise.ar();
                    var excitorEnv = EnvGen(Env.perc(\excitorAttack.ir(0.0), \excitorRelease.ir(0.1)));
                    var freq = \freq.ir(440).reciprocal;
                    var delayTimes = Array.geom(8, freq, \spread.ir(2));
                    var lag = \lag.kr(0.1);
                    var numDelays = 8;
                    var modulator = numDelays.collect{|i|
                        var s = SinOsc.kr(\modFreq.kr(1), rrand(0.0, 2pi));
                        s = s.abs;
                        s = s * \modDepth.kr(1);
                    };
                    delayTimes = delayTimes + modulator;
                    excitor * excitorEnv * \excitatorAmp.kr(0.2);
                    sig = numDelays.collect{|i|
                        CombC.ar(
                            excitor,
                            \maxDelay.ir(1),
                            delayTimes[i],
                            \decay.kr(1, lag),
                        ) / numDelays;
                    };
                    sig = sig.sum;
                    sig = BLowPass.ar(sig, \hicut.kr(4000, lag));
                },
            );

            synthDefPercWithoutPan = (
                pinkPerc: {|sig|
                    var lag = \lag.kr(0.1);
                    sig = PinkNoise.ar(); 
                    sig = BHiPass.ar(sig, \locut.kr(20, lag));
                    sig = BLowPass.ar(sig, \hicut.kr(20000, lag));
                    sig = sig * LFNoise1.ar(\modFreq.kr(10));
                }
            );

            synthDefPercWithPan.keysValuesDo{|name, func|
                MakeSynthDef(
                    name, 
                    func, 
                    env: \perc, 
                    pan: true, 
                    numOutputChannels: numChannels
                );
            };

            synthDefPercWithoutPan.keysValuesDo{|name, func|
                MakeSynthDef(
                    name, 
                    func, 
                    env: \perc, 
                    pan: false, 
                    numOutputChannels: numChannels
                );
            };

            synthDefAsrWithPan.keysValuesDo{|name, func|
                MakeSynthDef(
                    name, 
                    func, 
                    env: \asr, 
                    pan: true, 
                    numOutputChannels: numChannels
                );
            };

            synthDefAsrWithoutPan.keysValuesDo{|name, func|
                MakeSynthDef(
                    name, 
                    func, 
                    env: \asr, 
                    pan: false, 
                    numOutputChannels: numChannels
                );
            } 
        }
    }
