MBCellulator : KFMBDeltaTrig {

    var reverbTime = 2;

    *new{|db=0, speedlim=0.5, threshold=0.07, minAmp= -6, maxAmp=0, fadeTime=10|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBCellulator;
    }

    initMBCellulator {
        "MBCellulator initialized".postln;
        KF.loadBuffersSingleDir("/home/kf/mySamples/celloLublin/");
        fx.filter  (1, {|in|
            var inSplayed, delays, delayTimes, delayMix=0.2, numDelays=5, sig, verb, verbMix;
            var greyhole, greyholeMix;
            var lag = \lag.kr(0.1);
            in = in * 6.dbamp;
            inSplayed = SplayAz.ar(numDelays, in);
            delays = numDelays.collect{|i|
                CombC.ar( inSplayed[i], 1, LFNoise1.kr(rrand(0.1, 0.3)).range(0.2, 0.7), 1);
            };
            delays = SplayAz.ar(KF.numSpeakers, delays);
            delays = delays.rotate;
            sig = (1 - delayMix) * in + (delays * delayMix);

            // verbMix = LFNoise1.kr(1).range(0.0, 0.2);
            // sig = FreeVerb.ar(sig, verbMix, \room.kr(0.5));

            verb = JPverbMono.ar(sig, t60: \reverbTime.kr(reverbTime));
            verbMix = LFNoise1.kr(1).range(0.0, 0.2);
            sig = (1 - verbMix) * sig + (verb  * verbMix);

            greyhole = Greyhole.ar(
                sig, 
                \delayTime.kr(0.2, lag),
                \damp.kr(0, lag),
                \size.kr(1, lag),
                \diff.kr(0.707, lag),
                \feedback.kr(0.5), 
                \modDepth.kr(0.1), 
                \modFreq.kr(2.0)
            );
            greyholeMix = LFNoise1.kr(1).range(0.0, 0.2);
            sig = (1 - greyholeMix) * sig + (greyhole * greyholeMix);

            sig = BHiPass.ar(sig, LFNoise1.kr(0.1).exprange(80, 300));
            sig = BHiShelf.ar(sig, \hishelffreq.kr(1500, lag), db: \hishelfdb.kr(-12, lag));
            sig = sig * 18.dbamp;
            sig;
        } )
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            fx[0] = Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(KF.buf[\celloLublin]),
                \dur, dt.linlin(0.0, 1.0, 0.5, 3),
                \attack, Pkey(\dur) * 0.2,
                \release, Pkey(\dur) * 2,
                \rate, Prand([0.25, 0.5], inf),
                \rateDev, Pwhite(0.0, 0.5),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 2),
                \grainsize, Pwhite(0.01, 0.5),
                \grainfreq, Pwhite(2, 20),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \panDev, Pwhite(0.0, 1.0),
            );
        }
    }

    releaseAction {
        KF.freeBuffersSingleDir(\celloLublin);
    }
}

