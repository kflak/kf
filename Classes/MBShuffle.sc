MBShuffle : KFMBDeltaTrig {

    var <>buf = 0;
    var <>loop = false;
    var <currentPos;
    var <>grainSize = 0.1;
    var <>legato = 2;
    var <>releaseMul = 4;
    var <>minRate = 1.0;
    var <>maxRate = 1.0;
    var <>minPan = -1.0;
    var <>maxPan = 1.0;

    *new{|db=0, speedlim=0.3, threshold=0.01, minAmp= -6, maxAmp=0, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBShuffle;
    }

    initMBShuffle{
        KF.loadBuffersSingleDir("~/mySamples/arvoBetter/");
        currentPos = 0 ! KF.mb.size;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var buffer, numFrames, step, totalDuration, idx, pos;
            if(buf.isCollection){
                buffer = buf.choose;
            }{
                buffer = buf;
            };
            numFrames = buffer.numFrames;
            step = grainSize * KF.server.sampleRate;
            totalDuration = dt.linlin(0.0, 1.0, step, step * 5);
            idx = KF.mb.indexOf(id);
            pos = (currentPos[idx], currentPos[idx]+step..currentPos[idx]+totalDuration); 

            if(loop, 
            {
                currentPos[idx] = pos[pos.size-1].mod(numFrames); 
            },
            {
                currentPos[idx] = pos[pos.size-1]; 
            });

            fx[0] = Pbind(
                \instrument, \playbuf,
                \buf, buffer,
                \dur, grainSize,
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * releaseMul,
                \startPos, Pseq(pos),
                \legato, legato,
                \loop, 0,
                \rate, Pwhite(minRate, maxRate),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(minPan, maxPan),
                );
        };
    }
}

