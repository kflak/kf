// Superclass for other shuffleGranulators. Needs to define a buffer to 
// granulate.
MBShuffleGranulator : KFMBDeltaTrig {

    var <>buf = 0;
    var <>loop = false;
    var <currentPos;
    var <startPos;
    var <>tFreq = 20;
    var <>overlap = 2;
    var <>stride = 0.1;
    var <>legato = 1;
    var <>attackMul = 1;
    var <>releaseMul = 1;
    var <>minRate = 1.0;
    var <>maxRate = 1.0;
    var <>minRateModFreq = 0.0;
    var <>maxRateModFreq = 0.0;
    var <>minRateModDepth = 0.0;
    var <>maxRateModDepth = 0.0;
    var <>minPitch = 0.0; // in semitones
    var <>maxPitch = 0.0; // in semitones
    var <>minPitchModFreq = 0.0;
    var <>maxPitchModFreq = 0.0;
    var <>minPitchModDepth = 0.0;
    var <>maxPitchModDepth = 0.0;
    var <>minPan = -1.0;
    var <>maxPan = 1.0;
    var <>minDuration = 1.0;
    var <>maxDuration = 6.0;

    *new{|db=0, speedlim=0.3, threshold=0.01, minAmp= -6, maxAmp=0, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBShuffle;
    }

    initMBShuffle{
        currentPos = 0 ! KF.mb.size;
    }

    startPos_{|val| startPos = val; currentPos = startPos ! KF.mb.size }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var buffer, numFrames, step, totalDuration, idx, pos;
            if(buf.isCollection){
                buffer = buf.choose;
            }{
                buffer = buf;
            };
            numFrames = buffer.numFrames;
            step = stride * KF.server.sampleRate;
            totalDuration = dt.linlin(0.0, 1.0, minDuration, maxDuration);
            idx = KF.mb.indexOf(id);
            pos = currentPos[idx];
            currentPos[idx] = currentPos[idx] + step;

            if(loop) {
                currentPos[idx] = currentPos[idx].mod(numFrames); 
            };

            if(buffer.notNil){
                fx[0] = Pbind(
                    \instrument, \granulator,
                    \buf, buffer,
                    \dur, totalDuration,
                    \attack, Pkey(\dur) * attackMul,
                    \release, Pkey(\dur) * releaseMul,
                    \startPos, Pseq([pos]),
                    \legato, legato,
                    \loop, 0,
                    \tFreq, tFreq,
                    \overlap, overlap,
                    \posRate, Pwhite(minRate, maxRate),
                    \posRateModFreq, Pwhite(minRateModFreq, maxRateModFreq),
                    \posRateModDepth, Pwhite(minRateModDepth, maxRateModDepth),
                    \rate, Pwhite(minPitch, maxPitch).midiratio,
                    \rateModFreq, Pwhite(minPitchModFreq, maxPitchModFreq),
                    \rateModDepth, Pwhite(minPitchModDepth, maxPitchModDepth),
                    \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                    \pan, Pwhite(minPan, maxPan),
                );
            }
        };
    }
}

