MBDidgGranulated : KFMBDeltaTrig {

    var <>attackMul = 1;
    var <>releaseMul = 2;
    var <>minRate = 0.5;
    var <>maxRate = 0.5;
    var <>minRateModFreq = 0.01;
    var <>maxRateModFreq = 0.1;
    var <>minRateModDepth = 0.3;
    var <>maxRateModDepth = 0.9;
    var <>minPitch = 0.0; // in semitones
    var <>maxPitch = 0.0; // in semitones
    var <>minPitchModFreq = 0.0;
    var <>maxPitchModFreq = 0.0;
    var <>minPitchModDepth = 0.0;
    var <>maxPitchModDepth = 0.0;
    var <>minPan = 0.0;
    var <>maxPan = 0.0;

    *new{|db=0, speedlim=0.5, threshold=0.1, minAmp= -40, maxAmp=0, fadeTime=1|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeTime).initMBDidgGranulated;
    }

    initMBDidgGranulated {
        KF.loadBuffersSingleDir("~/mySamples/didg/");
        fx.filter (1, {|in|
            var lag = \lag.kr(0.1);
            var sig;
            var verbMix=0.3;
            var verb, locut, hishelf, out;
            in = in * \inputgain.kr(40.dbamp);
            verb = JPverb.ar(in, t60: 2);
            sig = (1 - verbMix) * in + (verb * verbMix);
            sig = Compander.ar(sig, sig, -10.dbamp, slopeAbove: 1/8);
            sig = sig.tanh;
            sig = BHiPass.ar(in: sig, freq: 80);
            // sig = BHiShelf.ar(in: sig, freq: 500, db: -3);
            sig = sig * \amp.kr(db.dbamp);
        })
    }


    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var buf = KF.buf[\didg];
            if(buf.notNil){
                fx[0] = Pbind(
                    \instrument, \granulator,
                    \buf, buf.choose,
                    \dur, dt.linlin(0.0, 1.0, 1, 4),
                    \attack, Pkey(\dur) * attackMul,
                    \release, Pkey(\dur) * releaseMul,
                    \startPos, 0,
                    \legato, 1,
                    \loop, 0,
                    \posRate, Pwhite(minRate, maxRate),
                    \posRateModFreq, Pwhite(minRateModFreq, maxRateModFreq),
                    \posRateModDepth, Pwhite(minRateModDepth, maxRateModDepth),
                    \rate, Pwhite(minPitch, maxPitch).midiratio,
                    \rateModFreq, Pwhite(minPitchModFreq, maxPitchModFreq),
                    \rateModDepth, Pwhite(minPitchModDepth, maxPitchModDepth),
                    \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                    \pan, Pwhite(minPan, maxPan, 1),
                );
            }
        }
    }

    releaseAction{
        KF.freeBuffersSingleDir(\didg);
    }
}
